package context;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBContext {

    private static DBContext instance;
    private static final String URL = "jdbc:mysql://localhost:3306/swp?useSSL=false&useUnicode=true&characterEncoding=UTF-8";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";

    private DBContext() {
    }

    public static DBContext getInstance() {
        if (DBContext.instance == null) {
            DBContext.instance = new DBContext();
        }
        return DBContext.instance;
    }

    static {
        try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException ex) {
            System.err.println("Không tìm thấy driver MySQL JDBC.");
            System.out.println(ex);
        }
    }

    public Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            return conn;
        } catch (SQLException ex) {
            throw new RuntimeException("Không thể kết nối đến cơ sở dữ liệu.", ex);
        }
    }

    public void close(ResultSet rs, PreparedStatement pstmt, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Đóng ResultSet thất bại.");
                System.out.println(ex);
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                System.err.println("Đóng PreparedStatement thất bại.");
                System.out.println(ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.err.println("Đóng kết nối cơ sở dữ liệu thất bại.");
                System.out.println(ex);
            }
        }
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import context.DBContext;
import entity.Account;
import entity.Token;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author quang
 */
public class DaoToken {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public Token getToken(String xtoken) {
        String xSql = "SELECT * FROM `token` WHERE `otp` = '" + xtoken + "'";
        Token x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Token(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("otp"),
                        rs.getTimestamp("time_expires"),
                        rs.getInt("type"),
                        rs.getInt("status"));

            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;

    }

    public void deleteToken(String xtoken) {
        String xSql = "DELETE FROM `token` WHERE otp = '" + xtoken + "'";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //type = 1 active type = 2 quên mật khẩu
    public Token insertToken(int xid, int xtoken, int type) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime() + 300000);
        String xSql = "INSERT INTO `token`(`id_account`, `otp`, `type`,`time_expires`) VALUES (?,?,?,?)";
        Token x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, xid);
            ps.setInt(2, xtoken);
            ps.setInt(3, type);
            ps.setTimestamp(4, timestamp);
            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {

        }
        return x;
    }

    public void updateToken(int xtoken) {
        String xSql = "UPDATE `token` SET `status`= ? WHERE id_account = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, "-1");
            ps.setInt(2, xtoken);
            ps.executeUpdate();
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateTokenActive(int xtoken) {
        String xSql = "UPDATE `token` SET `status`= ? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, "-1");
            ps.setInt(2, xtoken);
            ps.executeUpdate();
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
//    public static void main(String[] args) {
//        DaoToken a = new DaoToken();
////        a.updateToken("75");
////        a.insertToken(14, "1234", 1);
////        a.deleteToken("45548");
////        System.out.println(l.toString());
//    }
}

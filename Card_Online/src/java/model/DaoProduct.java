/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import context.DBContext;
import entity.CardValue;
import entity.CategoryProduct;
import entity.Product;
import entity.Stock;
import entity.TransHis;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quang
 */
public class DaoProduct {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public List<CategoryProduct> getCategory() {
        String xSql = "SELECT * FROM `category`";
        List<CategoryProduct> t = new ArrayList<>();
        CategoryProduct x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new CategoryProduct(
                        rs.getInt("id"),
                        rs.getString("namecat"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public List<CardValue> CardValue() {
        String xSql = "SELECT * FROM `category`";
        List<CardValue> t = new ArrayList<>();
        CardValue x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new CardValue(
                        rs.getInt("id"),
                        rs.getInt("money"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public List<Stock> getStockCode() {
        String xSql = "SELECT * FROM `stogre`";
        List<Stock> t = new ArrayList<>();
        Stock x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Stock(
                        rs.getInt("id"),
                        rs.getInt("id_product"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getString("seri"),
                        rs.getString("code"),
                        rs.getDate("date"),
                        rs.getDate("expiry"),
                        rs.getInt("status"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public void addStock(String xSeri, String xCode, String Name, String price, String xExpiry, String id_product) {
        String xSql = "INSERT INTO `stogre`(`seri`, `code`,`name`,`price`, `expiry`,`id_product`) VALUES (?,?,?,?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
//             ps.setString(1, id_product);
            ps.setString(1, xSeri);
            ps.setString(2, xCode);
            ps.setString(3, Name);
            ps.setString(4, price);
            ps.setString(5, xExpiry);
            ps.setString(6, id_product);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getTotalStock() {
        String xSql = "SELECT COUNT(*) FROM stogre ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalStock1() {
        String xSql = "SELECT COUNT(*) FROM stogre WHERE id_product = 1 ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalStock2() {
        String xSql = "SELECT COUNT(*) FROM stogre WHERE id_product =2";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalStock3() {
        String xSql = "SELECT COUNT(*) FROM stogre WHERE id_product =3 ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalStock4() {
        String xSql = "SELECT COUNT(*) FROM stogre WHERE id_product =4 ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalStock5() {
        String xSql = "SELECT COUNT(*) FROM stogre WHERE id_product =5 ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public void deleteStock(String xid) {
        String xSql = "DELETE FROM `stogre` WHERE id =" + xid;
        System.out.println(xSql);
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateStock(String id_product, String seri, String code, String name, String price, String date, String expiry, String id) {
        String xSql = "update `stogre` set `id_product` = ? ,`seri` = ? , `code` = ?,`name` = ?,`price` = ?, `date` = ?, `expiry` = ? where id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, id_product);
            ps.setString(2, seri);
            ps.setString(3, code);
            ps.setString(4, name);
            ps.setString(5, price);
            ps.setString(6, date);
            ps.setString(7, expiry);
            ps.setString(8, id);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Stock getStockByid(String id) {
        String xSql = "SELECT * FROM `stogre` WHERE id = ?";
        Stock x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Stock(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getDate(7),
                        rs.getDate(8),
                        rs.getInt(9));
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }

    public Product getProductByid(String id) {
        String xSql = "SELECT * FROM `product` WHERE id = ?";
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4));
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }

    public List<Product> getProduct() {
        String xSql = "SELECT * FROM `product` p, category c WHERE p.id_cate = c.id";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                         rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public Product selectProduct(String xId) {
        String xSql = "SELECT * FROM `product` p, category c WHERE p.id_cate = c.id and p.id =" + xId;
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                        rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;
    }

    public List<Product> pagingProduct(int xIndex, int xLimit) {
        String xSql = "SELECT * FROM `product` p, category c WHERE p.id_cate = c.id ORDER BY p.id  LIMIT ? OFFSET  ?";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();

            ps = con.prepareStatement(xSql);
            ps.setInt(1, xLimit);
            ps.setInt(2, (xIndex - 1) * xLimit);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                         rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public List<Product> searchProduct(int xIndex, int xLimit) {
        String xSql = "SELECT * FROM `product` p, category c WHERE p.id_cate = c.id ORDER BY p.id  LIMIT ? OFFSET  ?";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, xLimit);
            ps.setInt(2, (xIndex - 1) * xLimit);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                        rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public void deleteProduct(String xid) {
        String xSql = "DELETE FROM `product` WHERE id =" + xid;
        System.out.println(xSql);
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addProduct(String img, String Name, String id_cate) {
        String xSql = "INSERT INTO `product`(`img`,`name`,`id_cate`) VALUES (?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, img);
            ps.setString(2, Name);
            ps.setString(3, id_cate);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProduct(String img, String name, String type, String id) {
        String xSql = "update `product` set `img` = ?,`name` = ?, `id_cate` = ? where id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, img);
            ps.setString(2, name);
            ps.setString(3, type);
            ps.setString(4, id);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public int getTotalProduct() {
        String xSql = "SELECT  COUNT(*) FROM product";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalProductSearch(String key) {
        String xSql = "SELECT  COUNT(*) FROM product WHERE `name` LIKE ? ";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + key + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public List<Product> searchProductByName(int xLimit, int xIndex, String xTxt) {
        String xSql = "SELECT * FROM `product` p, category c WHERE p.id_cate = c.id and `name` LIKE '?' ORDER BY p.id  LIMIT ? OFFSET  ?";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, "%" + xTxt + "%");
            ps.setInt(2, xLimit);
            ps.setInt(3, (xIndex - 1) * xLimit);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                        rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return t;
    }

    public List<Product> getProduct1() {
        String xSql = "SELECT * FROM `product`";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                        rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public double getPrice(String xUser) {
        String xSql = "SELECT price FROM `product` WHERE `id` = ?";
        double money = 0;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, xUser);
            rs = ps.executeQuery();
            if (rs.next()) {
                money = rs.getDouble(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }
        return money;
    }

    public void ATC(String xId, int yId) {
        String xSql = "INSERT INTO `cart`(`id_product`, `id_account`) VALUES (?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, Integer.parseInt(xId));
            ps.setInt(2, yId);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Product> getCart(int xId) {
        String xSql = "SELECT p.id,p.name,p.price FROM `product` p JOIN\n"
                + "cart c on p.id=c.id_product\n"
                + "WHERE c.id_account=?";
        List<Product> t = new ArrayList<>();
        Product x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, xId);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Product(
                        rs.getInt("id"),
                        rs.getString("img"),
                        rs.getString("name"),
                        rs.getInt("id_cate"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public void delCart(String xid, int yid) {
        String xSql = "DELETE FROM `cart` WHERE id_product =" + xid + " and id_account=" + yid;
        System.out.println(xSql);
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        DaoProduct a = new DaoProduct();
//        List<Product> ok = a.getProduct1();
//        System.out.println(ok);
//         a.addProduct("", "vl", "5000", "54", "6");
         a.updateProduct("21", "ok", "1", "1");
//        DaoProduct a = new DaoProduct();
//        a.addStock("678", "987", "2023-02-20", "10");
//        a.updateStock("3", "2", "2023-03-20", "2020-09-20", "8");
//        int count = a.getTotalProduct()/5;
//        System.out.println(count);
//        List<Product> list = a.searchProductByName(5, 1, "2");
//        for (Product product : list) {
//            System.out.println(product);
//        }
//        System.out.println(a.getTotalProduct());
//        a.updateProduct("1", "1", "1", "1", "1", "1","114");
//        System.out.println(l.toString());
//        a.deleteProduct("101");
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import context.DBContext;
import entity.Account;
import entity.OrderCard;
import entity.Stock;
import entity.TransHis;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author quang
 */
public class DAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public List<OrderCard> getOrder() {
        String xSql = "select * from order_card where status = 0";
        List<OrderCard> t = new ArrayList<>();
        OrderCard x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new OrderCard(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("type_card"),
                        rs.getInt("value"),
                        rs.getInt("amount"),
                        rs.getInt("total_money"),
                        rs.getString("detail"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"),
                        rs.getInt("id_transaction_history"),
                        rs.getTimestamp("updatedAt")
                );
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return t;
    }

    public List<TransHis> getTransitionUserPaging(int id, int limit, int page, int price, int network, int status) {
        String xSql = "select * FROM transaction_history  WHERE id_account = ? AND (money = ? OR ? = 0)AND(status = ? OR ? = -1) AND (type = ? OR ? = -1) ORDER BY id LIMIT ? OFFSET ?";
        List<TransHis> t = new ArrayList<>();
        TransHis x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.setInt(2, price);
            ps.setInt(3, price);
            ps.setInt(4, network);
            ps.setInt(5, network);
            ps.setInt(6, status);
            ps.setInt(7, status);
            ps.setInt(8, limit);
            ps.setInt(9, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new TransHis(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getTimestamp("update_time"),
                        rs.getInt("money"),
                        rs.getInt("type"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    public List<TransHis> getTransitionAdminPaging(int limit, int page, int price, int network, int status) {
        String xSql = "select * FROM transaction_history  WHERE  (money = ? OR ? = 0)AND(status = ? OR ? = -1) AND (type = ? OR ? = -1) ORDER BY id LIMIT ? OFFSET ?";
        List<TransHis> t = new ArrayList<>();
        TransHis x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, price);
            ps.setInt(2, price);
            ps.setInt(3, network);
            ps.setInt(4, network);
            ps.setInt(5, status);
            ps.setInt(6, status);
            ps.setInt(7, limit);
            ps.setInt(8, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new TransHis(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getTimestamp("update_time"),
                        rs.getInt("money"),
                        rs.getInt("type"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }
    //status -1 là tất cả 0 là thành công 1 là Đang xử lý
    //Type -1 là tất cả 0 là Cộng tiền 1 là trừ tiền

    public static void main(String[] args) {
        DAO dao = new DAO();
        List<TransHis> o = dao.getTransitionAdminPaging(50, 1, 10000, 1, 2);
        System.out.println(o.toString());
    }
// 0 là tất cả 1 là bất bại 2 là thành công

    public List<OrderCard> getOrderPaging(int limit, int page, int price, int network, int status) {
        String xSql = "select * FROM order_card WHERE   (amount = ? OR ? = 0) AND (type_card = ? OR ? = 0) AND  (status <> ?) ORDER BY id LIMIT ? OFFSET ?";
        List<OrderCard> t = new ArrayList<>();
        OrderCard x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, price);
            ps.setInt(2, price);
            ps.setInt(3, network);
            ps.setInt(4, network);
            ps.setInt(5, status);
            ps.setInt(6, limit);
            ps.setInt(7, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new OrderCard(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("type_card"),
                        rs.getInt("value"),
                        rs.getInt("amount"),
                        rs.getInt("total_money"),
                        rs.getString("detail"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"),
                        rs.getInt("id_transaction_history"),
                        rs.getTimestamp("updatedAt")
                );
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return t;
    }

    // 0 là tất cả 1 là bất bại 2 là thành công
    public List<OrderCard> getOrderPagingAcc(int id, int limit, int page, int price, int network, int status) {
        String xSql = "select * FROM order_card WHERE id_account = ? AND (amount = ? OR ? = 0) AND (type_card = ? OR ? = 0) AND  (status <> ?) ORDER BY id LIMIT ? OFFSET ?";
        List<OrderCard> t = new ArrayList<>();
        OrderCard x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.setInt(2, price);
            ps.setInt(3, price);
            ps.setInt(4, network);
            ps.setInt(5, network);
            ps.setInt(6, status);
            ps.setInt(7, limit);
            ps.setInt(8, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new OrderCard(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("type_card"),
                        rs.getInt("value"),
                        rs.getInt("amount"),
                        rs.getInt("total_money"),
                        rs.getString("detail"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"),
                        rs.getInt("id_transaction_history"),
                        rs.getTimestamp("updatedAt")
                );
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return t;
    }

    public List<TransHis> getTransHis() {
        String xSql = "SELECT * FROM `transaction_history`";
        List<TransHis> t = new ArrayList<>();
        TransHis x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new TransHis(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getTimestamp("update_time"),
                        rs.getInt("money"),
                        rs.getInt("type"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt")
                );
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return t;
    }

    //type 1 là trừ tiền 2 là nạp tiền 3 là hoàn tiền 0 là 0
    //status 0 là đang xử lý 1 là thành công 2 là thất bại
    public void insertTransHis(Account a, int money, int type, int id) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        String xSql = "INSERT INTO `transaction_history`(`id_account`, `update_time`, `money`, `type`, `status`, `createdAt`,id) VALUES (?,?,?,?,?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, a.getId());
            ps.setTimestamp(2, timestamp);
            ps.setInt(3, money);
            ps.setInt(4, type);
            ps.setInt(5, 0);
            ps.setTimestamp(6, timestamp);
            ps.setInt(7, id);
            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {

        }
    }

    public void updateTransHis(int type, int status, int id) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        String xSql = "UPDATE `transaction_history` SET `update_time`= ?,`type`= ?,`status`= ? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setTimestamp(1, timestamp);
            ps.setInt(2, type);
            ps.setInt(3, status);
            ps.setInt(4, id);
            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {

        }
    }

    public int getTotalHis() {
        int total = 0;
        String xSql = "SELECT COUNT(*) FROM `transaction_history`";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery(); // Sử dụng executeQuery() thay vì executeUpdate()
            if (rs.next()) {
                total = rs.getInt(1);
            }
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total + 1;
    }

    public int getTotalStock(int price, String typeName) {
        int total = 0;
        String xSql = " SELECT COUNT(*) FROM stogre WHERE price = ? AND `status` = ? AND `name`= ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, price); // truyền giá trị 20000 vào tham số đầu tiên
            ps.setInt(2, 0); // truyền giá trị 0 vào tham số 
            ps.setString(3, typeName); // truyền giá trị "Viettel" vào tham số thứ ba
            rs = ps.executeQuery(); // Sử dụng executeQuery() thay vì executeUpdate()
            if (rs.next()) {
                total = rs.getInt(1);
            }
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }

    public List<Stock> getStockForOrder(int price, String typeName) {
        String xSql = "SELECT * FROM stogre WHERE price = ? AND `status` = ? AND `name`= ?";
        List<Stock> t = new ArrayList<>();
        Stock x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, price); // truyền giá trị 20000 vào tham số đầu tiên
            ps.setInt(2, 0); // truyền giá trị 0 vào tham số 
            ps.setString(3, "viettel"); // truyền giá trị "Viettel" vào tham số thứ ba
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Stock(
                        rs.getInt("id"),
                        rs.getInt("id_product"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getString("seri"),
                        rs.getString("code"),
                        rs.getDate("date"),
                        rs.getDate("expiry"),
                        rs.getInt("status"));
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public int getTotalStockPaging() {
        int total = 0;
        String xSql = "SELECT COUNT(*) FROM stogre s";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery(); // Sử dụng executeQuery() thay vì executeUpdate()
            if (rs.next()) {
                total = rs.getInt(1);
            }
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }

    //0 là đang xử lý 1 là thành công 2 là thất bại
    public void insertOrder(Account a, int card, int totalMoney, int amount, int type, int idTrans) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        String xSql = "INSERT INTO `order_card`(`id_account`, `type_card`, `value`, `amount`, `total_money`, `detail`, `createdAt`, `id_transaction_history`,status) "
                + "VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, a.getId());
            ps.setInt(2, card);
            ps.setInt(3, type);
            ps.setInt(4, amount);
            ps.setInt(5, totalMoney);
            ps.setString(6, "[]");
            ps.setTimestamp(7, timestamp);
            ps.setInt(8, idTrans);
            ps.setInt(9, 0);
            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateOrder(int id, String detail, int type) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        String xSql = "UPDATE `order_card` SET `detail`=?,`status`=?,`updatedAt`=? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, detail);
            ps.setInt(2, type);
            ps.setTimestamp(3, timestamp);
            ps.setInt(4, id);
            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //-1 là là cả 2 0 là dùng rồi 1 là chưa dùng
    public List<Stock> getStockPagingAjax(int limit, int page, int price, int network, int status) {
        String xSql = "SELECT * FROM stogre\n"
                + "WHERE \n"
                + "    (price = ? OR ? = 0) AND\n"
                + "    (id_product = ? OR ? = 0) AND\n"
                + "    (status <> ?)\n"
                + "ORDER BY id\n"
                + "LIMIT ? OFFSET ?";
        List<Stock> t = new ArrayList<>();
        Stock x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, price);
            ps.setInt(2, price);
            ps.setInt(3, network);
            ps.setInt(4, network);
            ps.setInt(5, status);
            ps.setInt(6, limit);
            ps.setInt(7, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Stock(
                        rs.getInt("id"),
                        rs.getInt("id_product"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getString("seri"),
                        rs.getString("code"),
                        rs.getDate("date"),
                        rs.getDate("expiry"),
                        rs.getInt("status"));
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public List<Stock> getStockPaging(int limit, int page) {
        String xSql = "SELECT * FROM `stogre` ORDER BY id  LIMIT ? OFFSET  ?;";
        List<Stock> t = new ArrayList<>();
        Stock x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, limit);
            ps.setInt(2, page);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Stock(
                        rs.getInt("id"),
                        rs.getInt("id_product"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getString("seri"),
                        rs.getString("code"),
                        rs.getDate("date"),
                        rs.getDate("expiry"),
                        rs.getInt("status"));
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public void updateStock(int type, int id) throws ParseException {
        String xSql = "UPDATE `stogre` SET `status`= ? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, type);
            ps.setInt(2, id);

            ps.executeUpdate();
            rs.close();
            ps.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addStock(Stock stk) throws ParseException {
        Date strDate = stk.getExpiry();
        Timestamp timestamp = new Timestamp(strDate.getTime());
        String xSql = "INSERT INTO `stogre`(`id_product`,`seri`, `code`,`name`,`price`, `expiry`,`status`) VALUES (?,?,?,?,?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, stk.getId_product());
            ps.setString(2, stk.getSeri());
            ps.setString(3, stk.getCode());
            ps.setString(4, stk.getName());
            ps.setInt(5, stk.getPrice());
            ps.setTimestamp(6, timestamp);
            ps.setInt(7, 0);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Stock> getStock() {
        String xSql = "SELECT * FROM `stogre` where status = 0";
        List<Stock> t = new ArrayList<>();
        Stock x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Stock(
                        rs.getInt("id"),
                        rs.getInt("id_product"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getString("seri"),
                        rs.getString("code"),
                        rs.getDate("date"),
                        rs.getDate("expiry"),
                        rs.getInt("status"));
                t.add(x);
            }
            rs.close();
            ps.close();
            con.close();
            DBContext.getInstance().close(rs, ps, con);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

//    public static void main(String[] args) throws ParseException {
//        DaoAccount daoAccount = new DaoAccount();
//        DAO dao = new DAO();
//        List<OrderCard> o = dao.getOrder();
//        System.out.println(o.toString());
//        dao.addStock("1", "1", "1", "1", "2033-06-28 14:30:05.987", "1");
////        System.out.println(dao.getTotalStock(0, "viettel"));
////        Account a = daoAccount.getUserbyId("60");
////        dao.insertOrder(a, 10000, 200000,5,1,5);
//    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Account;
import context.DBContext;
import entity.OrderCard;
import entity.TransHis;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author LEGION
 */
public class DaoAccount {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public List<Account> getUsers() {
        String xSql = "select * from account";
        List<Account> t = new ArrayList<>();
        Account x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public List<TransHis> getTransition(int pid) {
        String xSql = "SELECT * FROM `transaction_history` WHERE id_account = " + pid;
        List<TransHis> t = new ArrayList<>();
        TransHis x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new TransHis(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getTimestamp("update_time"),
                        rs.getInt("money"),
                        rs.getInt("type"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    public List<TransHis> getTransitionAdmin() {
        String xSql = "SELECT * FROM `transaction_history`";
        List<TransHis> t = new ArrayList<>();
        TransHis x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new TransHis(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getTimestamp("update_time"),
                        rs.getInt("money"),
                        rs.getInt("type"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    public List<OrderCard> getOrderAdmin() {
        String xSql = "SELECT * FROM `order_card`";
        List<OrderCard> t = new ArrayList<>();
        OrderCard x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new OrderCard(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("type_card"),
                        rs.getInt("value"),
                        rs.getInt("amount"),
                        rs.getInt("total_money"),
                        rs.getString("detail"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"),
                        rs.getInt("id_transaction_history"),
                        rs.getTimestamp("updatedAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    public List<OrderCard> getOrderUser(int pid) {
        String xSql = "SELECT * FROM `order_card` where id_account = " + pid;
        List<OrderCard> t = new ArrayList<>();
        OrderCard x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new OrderCard(
                        rs.getInt("id"),
                        rs.getInt("id_account"),
                        rs.getInt("type_card"),
                        rs.getInt("value"),
                        rs.getInt("amount"),
                        rs.getInt("total_money"),
                        rs.getString("detail"),
                        rs.getInt("status"),
                        rs.getTimestamp("createdAt"),
                        rs.getInt("id_transaction_history"),
                        rs.getTimestamp("updatedAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return t;
    }

    public List<Account> PagingAccount(int xIndex, int xLimit) {
        String xSql = "SELECT * FROM account a ORDER BY a.id  LIMIT ? OFFSET ?";
        List<Account> t = new ArrayList<>();
        Account x;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, xLimit);
            ps.setInt(2, (xIndex - 1) * xLimit);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
                t.add(x);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return t;
    }

    public Account getUser(String xName, String xPass) {
        String xSql = "select * from account where user = '" + xName + "' and password = '" + xPass + "'";
        Account x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;

    }

    public Account checkUserbyName(String xName) {
        String xSql = "select * from account where user = '" + xName + "'";
        Account x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;
    }

    public Account checkUserbyMail(String xMail) {
        String xSql = "select * from account where email = '" + xMail + "'";
        Account x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;
    }

    public void insertUser(String xName, String xPass, String xEmail) {
        String xSql = "INSERT INTO `account` (`user`, `password`,`email`) VALUES (?,?,?)";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, xName);
            ps.setString(2, xPass);
            ps.setString(3, xEmail);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public void updatePass(String xPass, int xId) {
        String xSql = "UPDATE `account` SET `password`='" + xPass + "' WHERE id = '" + xId + "'";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public void updateActive(int xActive) {
        String xSql = "UPDATE `account` SET `isActive`='1' WHERE `id` = '" + xActive + "'";

        Account x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public void updateInfo(String xName, String xAge, String xAddress, int xId) {
        String xSql = "UPDATE `account` SET `name`='" + xName + "',`age`='" + xAge + "',`address`='" + xAddress + "'WHERE id = " + xId;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public void updateAcc(String xName, String xPass, String xEmail, String xMoney, String isAdmin, String isActive, String xId) {
        String xSql = "UPDATE `account` SET `user`= ?,`password`=?,`email`=? ,`isAdmin`=?,`isActive`=?,`money`=? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, xName);
            ps.setString(2, xPass);
            ps.setString(3, xEmail);
            ps.setString(4, isAdmin);
            ps.setString(5, isActive);
            ps.setString(6, xMoney);
            ps.setString(7, xId);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public Account getUserbyId(int xId) {
        String xSql = "SELECT * FROM `account` WHERE `id` = ?";
        Account x = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, xId);
            rs = ps.executeQuery();
            while (rs.next()) {
                x = new Account(
                        rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getInt("ban"),
                        rs.getInt("isAdmin"),
                        rs.getInt("isActive"),
                        rs.getInt("money"),
                        rs.getDate("createdAt"));
            }
            DBContext.getInstance().close(rs, ps, con);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return x;

    }

    public void deletebyName(String xName) {
        String xSql = null;
        Account x = null;

    }

    public void deletebyId(String id) {
        String xSql = "DELETE FROM `account` WHERE id =" + id;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
    }

    public int getTotalAccount() {
        String xSql = "SELECT  COUNT(*) FROM account";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {

        }
        return 0;
    }

    public int getMoney(String xUser) {
        String xSql = "SELECT money FROM `account` WHERE `user` = ?";
        int money = 0;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, xUser);
            rs = ps.executeQuery();
            if (rs.next()) {
                money = rs.getInt(1);
            }
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }
        return money;
    }

    public void subMoney(Account a, int money) {
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        String xSql = "UPDATE `account` SET `money`=(money-?),`updatedAt`=? WHERE id = ?";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, money);
            ps.setTimestamp(2, timestamp);
            ps.setInt(3, a.getId());
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }

    }

    public String paid(String xName, double price, String id) {
        String alert = "";
        double money = getMoney(xName);
        if (price > money) {
            alert = "Không đủ số dư để thanh toán!";
        } else {
            String xSql = "UPDATE `account` SET `money`=? WHERE user = ?";
            alert = "Thanh toán thành công";
            DaoProduct p = new DaoProduct();
            ATT(id, getIdByAccount(xName), price);
            p.delCart(id, getIdByAccount(xName));
            try {
                con = DBContext.getInstance().getConnection();
                ps = con.prepareStatement(xSql);
                ps.setDouble(1, money - price);
                ps.setString(2, xName);
                ps.executeUpdate();
                DBContext.getInstance().close(rs, ps, con);

            } catch (Exception e) {
            }
        }
        return alert;
    }

    public int getAccountById(int id) {
        String xSql = "SELECT * FROM `account` WHERE `id` = ?";
        int money = 0;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                money = rs.getInt(1);
            }

            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }
        return money;
    }

    public int getIdByAccount(String Acc) {
        String xSql = "SELECT id FROM `account` WHERE `user` = ?";
        int money = 0;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, Acc);
            rs = ps.executeQuery();
            if (rs.next()) {
                money = rs.getInt(1);
            }

            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }
        return money;
    }

    public void ATT(String xid, int yid, double money) {
        String xSql = "INSERT INTO `transaction_history`(`id_product`, `id_account`, `date`, `money`) VALUES (" + xid + "," + yid + ",?," + money + ")";
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            String n = java.time.LocalDate.now().toString() + " ";
            n += java.time.LocalTime.now().toString();
            ps.setString(1, n);
            ps.executeUpdate();
            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPassByAccount(String Acc) {
        String xSql = "SELECT password FROM `account` WHERE `user` = ?";
        String money = null;
        try {
            con = DBContext.getInstance().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, Acc);
            rs = ps.executeQuery();
            if (rs.next()) {
                money = rs.getString(1);
            }

            DBContext.getInstance().close(rs, ps, con);

        } catch (Exception e) {
        }
        return money;
    }

    public static void main(String[] args) {
//        DaoAccount a = new DaoAccount();
//        List<OrderCard> ok=a.getOrderAdmin();
//        System.out.println(ok);
//        a.subMoney(a.getUserbyId("61"), 10000);
////        List<Account> acc = a.PagingAccount(1, 5);
////        System.out.println(acc.toString());
////        a.updateAcc("1", "1", "1", "1", "1", "3", "42");
////        a.deletebyId("45");
////        a.insertUser("duyen11232", "123456", "duyen2k2@gmail.com");
////        List<Account> l = a.getUsers();
////            a.updatePass("123451", "14");
////a.updateActive("14");
////        Account l = a.getUserbyId("14");
////    a.updateInfo("1323", "45");
////        Account l = a.checkUserbyName("quang");
////        System.out.println(l.toString());
    }

}

package service;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;
import javax.imageio.ImageIO;

public class CaptchaToHtml {

    private static final int WIDTH = 200;
    private static final int HEIGHT = 50;
    private static final int FONT_SIZE = 30;
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int CHARACTERS_LENGTH = CHARACTERS.length();
    private static final int CODE_LENGTH = 6;
    private static final int LINE_COUNT = 10;
    private static final int LINE_THICKNESS = 2;

    public static String[] CaptchaToHtml() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        Random random = new Random();
        for (int i = 0; i < LINE_COUNT; i++) {
            g.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            g.drawLine(random.nextInt(WIDTH), random.nextInt(HEIGHT), random.nextInt(WIDTH), random.nextInt(HEIGHT));
        }
        StringBuilder code = new StringBuilder();
        Font font = new Font("Arial", Font.BOLD, FONT_SIZE);
        g.setFont(font);
        for (int i = 0; i < CODE_LENGTH; i++) {
            char c = CHARACTERS.charAt(random.nextInt(CHARACTERS_LENGTH));
            code.append(c);
            g.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            g.drawString(Character.toString(c), i * WIDTH / CODE_LENGTH, HEIGHT / 2 + FONT_SIZE / 2);
        }
        for (int i = 0; i < LINE_COUNT; i++) {
            g.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            g.drawLine(random.nextInt(WIDTH), random.nextInt(HEIGHT), random.nextInt(WIDTH), random.nextInt(HEIGHT));
        }
        g.dispose();
        String captchaCode = code.toString();
        String base64Image = encodeImage(image, "png");
        String[] string = {base64Image, captchaCode};
        return string;
        // TODO: Save the image and the code
    }


    public static String encodeImage(BufferedImage image, String formatName) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, formatName, baos);
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (IOException e) {
            System.err.println("Error encoding image");
            return null;
        }
    }

}

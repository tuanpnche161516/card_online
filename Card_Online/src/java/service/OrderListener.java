package service;

import com.google.gson.Gson;
import entity.Account;
import entity.OrderCard;
import entity.Stock;
import java.util.Queue;
import java.util.LinkedList;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.DaoAccount;

@WebListener
public class OrderListener implements ServletContextListener {

    private final DAO dao = new DAO();
    private final Queue<OrderCard> orderQueue = new LinkedList<>(); // Khởi tạo queue mới

    @Override
    public void contextInitialized(ServletContextEvent event) {
        SendMailBySite st = new SendMailBySite();
        DaoAccount daoAccount = new DaoAccount();
        Thread thread;
        thread = new Thread(() -> {
            while (true) {
                List<OrderCard> orders = dao.getOrder();
                for (OrderCard order : orders) {
                    orderQueue.offer(order); // Thêm đơn hàng vào queue
                }
                while (!orderQueue.isEmpty()) {
                    OrderCard order = orderQueue.poll(); // Lấy đơn hàng đầu tiên trong queue
                    String type = null;
                    switch (order.getTypeCard()) {
                        case 1:
                            type = "Viettel";
                            break;
                        case 2:
                            type = "Mobifone";
                            break;
                        case 3:
                            type = "vinaphone";
                            break;
                        case 4:
                            type = "Vietnamobile";
                            break;
                        case 5:
                            type = "gate";
                            break;
                        case 6:
                            type = "zing";
                            break;
                        default:
                            break;
                    }
                    int remaining = dao.getTotalStock(order.getAmount(), type);
                    if (remaining < order.getValue()) {
                        System.out.println("Số thẻ không đủ");
                        dao.updateOrder(order.getId(), "[]", 2);
                    } else if (remaining >= order.getValue()) {
                        List<Stock> stk = dao.getStockForOrder(order.getAmount(), type);
                        List<Stock> stkSubset = new ArrayList<>();
                        int c = 0;
                        for (Stock s : stk) {
                            if (c == order.getValue()) {
                                break;
                            }
                            Stock subset = new Stock();
                            subset.setId(s.getId());
                            subset.setId_product(order.getTypeCard());
                            subset.setPrice(s.getPrice());
                            subset.setSeri(s.getSeri());
                            subset.setCode(s.getCode());
                            stkSubset.add(subset);
                            try {
                                dao.updateStock(1, s.getId());
                            } catch (ParseException ex) {
                                Logger.getLogger(OrderListener.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            c++;
                        }
                        Gson gson = new Gson();
                        Account a = daoAccount.getUserbyId(order.getIdAccount());
                        String jsonStk = gson.toJson(stkSubset);
                        System.out.println(a.getEmail());
                        st.sendMailCard(a.getEmail(), stkSubset);
                        dao.updateOrder(order.getId(), jsonStk, 1);
                    }
                }
                try {
                    System.out.println("Loading Again Order"); // In ra thông tin đơn hàng
                    Thread.sleep(50000); // Đợi 5 giây trước khi chạy lại
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
        });
        thread.start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // Không cần thực hiện gì khi ứng dụng web bị hủy
    }
}

package service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import entity.Stock;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.SplittableRandom;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.DAO;
import model.DaoToken;

public class SendMailBySite {

    public static void send(String to, String sub, String msg, final String user, final String pass) {
        Properties props = new Properties();
        props.put("mail.smtp.user", user);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.EnableSSL.enable", "true");

        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            InternetAddress[] toAddresses = {new InternetAddress(to)};
            message.setRecipients(Message.RecipientType.TO, toAddresses);
            message.setSubject(sub, "UTF-8");
            message.setSentDate(new Date());
//            message.setText(msg);
            message.setContent(msg, "text/html; charset=UTF-8"); // Cài đặt nội dung bằng mã hóa UTF-8
            Transport.send(message);

            System.out.println("thanh cong");
        } catch (MessagingException e) {
            System.out.println(e);
        }
    }

    public static String generateOtp(int otpLenght) {
        SplittableRandom splittableRandom = new SplittableRandom();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < otpLenght; i++) {
            sb.append(splittableRandom.nextInt(0, 10));
        }
        return sb.toString();
    }

    public void sendMailActive(String to, int id) {
        int otp = Integer.parseInt(generateOtp(5));
        DaoToken daoToken = new DaoToken();
        daoToken.insertToken(id, otp, 1);
        for (int i = 0; i < 1; i++) {
            String sub = "Mã xác nhận kích hoạt tài khoản của bạn";
            String message = "<!DOCTYPE html>\n"
                    + "<html lang=\"vn\">\n"
                    + "<head>\n"
                    + "    <meta charset=\"UTF-8\">\n"
                    + "    <title>Mã OTP của bạn</title>\n"
                    + "    <style>\n"
                    + "        body {\n"
                    + "            font-family: Arial, sans-serif;\n"
                    + "            font-size: 16px;\n"
                    + "            color: #333;\n"
                    + "        }\n"
                    + "        h1 {\n"
                    + "            font-size: 24px;\n"
                    + "            color: #0066cc;\n"
                    + "        }\n"
                    + "        p {\n"
                    + "            font-size: 20px;\n"
                    + "            color: #333;\n"
                    + "        }\n"
                    + "    </style>\n"
                    + "</head>\n"
                    + "<body>\n"
                    + "    <h1>Mã OTP của bạn là:</h1>\n"
                    + "    <p>" + otp + "</p>\n"
                    + "</body>\n"
                    + "</html>";
            Runnable runnable = () -> {
                SendMailBySite.send(to, sub, message, "cardonlinese1701@gmail.com", "pldiympaewqsccom");
            };
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }

//    public static void main(String[] args) {
//        SendMailBySite st = new SendMailBySite();
//        DAO dao = new DAO();
//        List<Stock> stk = dao.getStockForOrder(10000, "viettel");
//        st.sendMailCard("quang020102@gmail.com", stk);
//    }
    public void sendMailCard(String to, List<Stock> obj) {
        String sub = "Đơn Hàng của bạn";
        String message = "<!DOCTYPE html>\n"
                + "<html lang=\"vn\">\n"
                + "<head>\n"
                + "    <meta charset=\"UTF-8\">\n"
                + "    <title>Mã OTP của bạn</title>\n"
                + "    <style>\n"
                + "        body {\n"
                + "            font-family: Arial, sans-serif;\n"
                + "            font-size: 16px;\n"
                + "            color: #333;\n"
                + "        }\n"
                + "        h1 {\n"
                + "            font-size: 24px;\n"
                + "            color: #0066cc;\n"
                + "        }\n"
                + "        p {\n"
                + "            font-size: 20px;\n"
                + "            color: #333;\n"
                + "        }\n"
                + "    </style>\n"
                + "</head>\n"
                + "<body>\n"
                + "    <h1>Thẻ của bạn:</h1>\n";
        for (Stock stock : obj) {
            message += "    <p> seri: " + stock.getSeri() + "</p>\n"
                    + "    <p> code: " + stock.getCode() + "</p>\n"
                    + "    <p> mệnh giá: " + stock.getPrice() + "</p>\n";
        }

        message += "</body>\n"
                + "</html>";
        String last = message;
        Runnable runnable = () -> {
            SendMailBySite.send(to, sub, last, "cardonlinese1701@gmail.com", "pldiympaewqsccom");
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void sendMailOTPPass(String to, int id) {
        int otp = Integer.parseInt(generateOtp(5));
        DaoToken daoToken = new DaoToken();
        daoToken.insertToken(id, otp, 2);
        for (int i = 0; i < 1; i++) {
            String sub = "Mã xác nhận kích hoạt tài khoản của bạn";
            String message = "<!DOCTYPE html>\n"
                    + "<html lang=\"vn\">\n"
                    + "<head>\n"
                    + "    <meta charset=\"UTF-8\">\n"
                    + "    <title>Mã OTP của bạn</title>\n"
                    + "    <style>\n"
                    + "        body {\n"
                    + "            font-family: Arial, sans-serif;\n"
                    + "            font-size: 16px;\n"
                    + "            color: #333;\n"
                    + "        }\n"
                    + "        h1 {\n"
                    + "            font-size: 24px;\n"
                    + "            color: #0066cc;\n"
                    + "        }\n"
                    + "        p {\n"
                    + "            font-size: 20px;\n"
                    + "            color: #333;\n"
                    + "        }\n"
                    + "    </style>\n"
                    + "</head>\n"
                    + "<body>\n"
                    + "    <h1>Mã OTP của bạn là:</h1>\n"
                    + "    <p>" + otp + "</p>\n"
                    + "</body>\n"
                    + "</html>";
            Runnable runnable = () -> {
                SendMailBySite.send(to, sub, message, "cardonlinese1701@gmail.com", "pldiympaewqsccom");
            };
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }
}

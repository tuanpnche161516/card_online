/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import entity.Stock;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.SplittableRandom;
import model.DaoProduct;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author quang
 */
public class Util {

    public String generateOtp(int otpLenght) {
        SplittableRandom splittableRandom = new SplittableRandom();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < otpLenght; i++) {
            sb.append(splittableRandom.nextInt(0, 10));
        }
        return sb.toString();
    }

    public String getTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date date = new Date();

        return formatter.format(date);
    }

    public String detailOrder(int value, int amount, String name) {
        int count = 0;
        DaoProduct daoProduct = new DaoProduct();
        List<Stock> stk = daoProduct.getStockCode();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        for (Stock stock : stk) {
            if (count == amount) {
                break;
            }
            if (value == stock.getPrice() && stock.getName().equalsIgnoreCase(name) && stock.getStatus() == 0) {
                jsonObject.put("id", stock.getId());
                jsonObject.put("price", stock.getPrice());
                jsonObject.put("seri", stock.getSeri());
                jsonObject.put("code", stock.getCode());
                jsonObject.put("expired date", stock.getExpiry());
                jsonArray.add(jsonObject);
                count++;
            }
        }

        String jsonString = JSONArray.toJSONString(jsonArray);
        return jsonString;
    }

    public void importExcel(Workbook workbook, InputStream fileContent) {

    }
//    public static void main(String[] args) {
//
//    }
}

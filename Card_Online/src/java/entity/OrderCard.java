/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Timestamp;

/**
 *
 * @author quang
 */
public class OrderCard {

    private int id;
    private int idAccount;
    private int typeCard;
    private int value;
    private int amount;
    private int total;
    private String detail;
    private int status;
    private Timestamp createdAt;
    private int idTrans;
    private Timestamp updatedAt;

    public OrderCard() {
    }

    public OrderCard(int id, int idAccount, int typeCard, int value, int amount, int total, String detail, int status, Timestamp createdAt, int idTrans, Timestamp updatedAt) {
        this.id = id;
        this.idAccount = idAccount;
        this.typeCard = typeCard;
        this.value = value;
        this.amount = amount;
        this.total = total;
        this.detail = detail;
        this.status = status;
        this.createdAt = createdAt;
        this.idTrans = idTrans;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getTypeCard() {
        return typeCard;
    }

    public void setTypeCard(int typeCard) {
        this.typeCard = typeCard;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public int getIdTrans() {
        return idTrans;
    }

    public void setIdTrans(int idTrans) {
        this.idTrans = idTrans;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "OrderCard{" + "id=" + id + ", idAccount=" + idAccount + ", typeCard=" + typeCard + ", value=" + value + ", amount=" + amount + ", total=" + total + ", detail=" + detail + ", status=" + status + ", createdAt=" + createdAt + ", idTrans=" + idTrans + ", updatedAt=" + updatedAt + '}';
    }

}

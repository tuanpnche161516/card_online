/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;

/**
 *
 * @author LEGION
 */
public class Account {

    private int id;
    private String user;
    private String password;
    private String email;
    private int ban;
    private int isAdmin;
    private int isActive;
    private int money;
    private Date createAt;

    public Account() {
    }

    public Account(int id, String user, String password, String email, int ban, int isAdmin, int isActive, int money, Date createAt) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.email = email;
        this.ban = ban;
        this.isAdmin = isAdmin;
        this.isActive = isActive;
        this.money = money;
        this.createAt = createAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBan() {
        return ban;
    }

    public void setBan(int ban) {
        this.ban = ban;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", user=" + user + ", password=" + password + ", email=" + email + ", ban=" + ban + ", isAdmin=" + isAdmin + ", isActive=" + isActive + ", money=" + money + ", createAt=" + createAt + '}';
    }

}

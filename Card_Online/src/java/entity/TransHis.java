/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Timestamp;

/**
 *
 * @author quang
 */
public class TransHis {
    private int id;
    private int id_account;
    private Timestamp update_time;
    private int money;
    private int type;
    private int status;
    private Timestamp createdAt;

    public TransHis() {
    }

    public TransHis(int id, int id_account, Timestamp update_time, int money, int type, int status, Timestamp createdAt) {
        this.id = id;
        this.id_account = id_account;
        this.update_time = update_time;
        this.money = money;
        this.type = type;
        this.status = status;
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public Timestamp getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Timestamp update_time) {
        this.update_time = update_time;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "TransHis{" + "id=" + id + ", id_account=" + id_account + ", update_time=" + update_time + ", money=" + money + ", type=" + type + ", status=" + status + ", createdAt=" + createdAt + '}';
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author quang
 */
public class Product {

    private int id;
    private String img;
    private String name;
    private int id_cate;

    public Product() {
    }

    public Product(int id, String img, String name, int id_cate) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.id_cate = id_cate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_cate() {
        return id_cate;
    }

    public void setId_cate(int id_cate) {
        this.id_cate = id_cate;
    }

    

  

    

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", img=" + img + ", name=" + name  + ", id_cate=" + id_cate + '}';
    }

}

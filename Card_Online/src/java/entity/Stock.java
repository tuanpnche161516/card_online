/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;

/**
 *
 * @author quang
 */
public class Stock {

    private int id;
    private int id_product;
    private String name;
    private int price;
    private String seri;
    private String code;
    private Date date;
    private Date expiry;
    private int status;

    public Stock() {
    }

    public Stock(int id, int id_product, String name, int price, String seri, String code, Date date, Date expiry, int status) {
        this.id = id;
        this.id_product = id_product;
        this.name = name;
        this.price = price;
        this.seri = seri;
        this.code = code;
        this.date = date;
        this.expiry = expiry;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSeri() {
        return seri;
    }

    public void setSeri(String seri) {
        this.seri = seri;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", id_product=" + id_product + ", name=" + name + ", price=" + price + ", seri=" + seri + ", code=" + code + ", date=" + date + ", expiry=" + expiry + ", status=" + status + '}';
    }

}

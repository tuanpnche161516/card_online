/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Timestamp;

/**
 *
 * @author quang
 */
public class Token {

    private int id;
    private int id_account;
    private int otp;
    private Timestamp date;
    private int type;
    private int status;

    public Token() {
    }

    public Token(int id, int id_account, int otp, Timestamp date, int type, int status) {
        this.id = id;
        this.id_account = id_account;
        this.otp = otp;
        this.date = date;
        this.type = type;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Token{" + "id=" + id + ", id_account=" + id_account + ", otp=" + otp + ", date=" + date + ", type=" + type + ", status=" + status + '}';
    }

}

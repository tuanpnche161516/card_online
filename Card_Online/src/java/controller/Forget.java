/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.DaoAccount;
import model.DaoToken;
import service.SendMailBySite;
import service.Util;

/**
 *
 * @author quang
 */
@WebServlet(name = "forget", urlPatterns = {"/forget"})
public class Forget extends HttpServlet {

    private static final long serialVersionUID = 1L;
    public String captcha;
    private String recapcha;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.getRequestDispatcher("forget.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        recapcha = request.getParameter("recaptcha");
        captcha = (String) session.getAttribute("captcha");
        DaoToken token = new DaoToken();
        if (recapcha != null && recapcha.length() != 0) {
            if (recapcha.equalsIgnoreCase(captcha) && name != null && name.length() != 0) {
                SendMailBySite m = new SendMailBySite();
                DaoAccount daoAccount = new DaoAccount();
                Account a = daoAccount.checkUserbyName(name);
                token.updateToken(a.getId());
                m.sendMailOTPPass(a.getEmail(), a.getId());
                session.setAttribute("AccountChangePass", a);
                response.sendRedirect("changepass");
                return;
            } else {
                request.setAttribute("arlet", "Captcha không chính xác");
            }
        }
        request.setAttribute("name", name);
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

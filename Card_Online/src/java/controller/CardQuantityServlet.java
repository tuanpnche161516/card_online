package controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.DAO;

@WebServlet(name = "card_quantity", urlPatterns = {"/card_quantity"})

public class CardQuantityServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Lấy giá trị nhà mạng và mệnh giá từ yêu cầu GET
        String network = request.getParameter("network");
        String amount = request.getParameter("amount");
        DAO dao = new DAO();
        // Giả sử rằng số lượng thẻ còn lại được lấy từ cơ sở dữ liệu
        int quantity = dao.getTotalStock(Integer.parseInt(amount), network);
        // Trả về kết quả dưới dạng JSON
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("{\"quantity\":" + quantity + "}");
        out.flush();
    }
}

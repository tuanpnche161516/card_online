/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import entity.TransHis;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.DaoAccount;

/**
 *
 * @author quang
 */
@WebServlet(name = "addorder", urlPatterns = {"/addorder"})
public class AddOrder extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, InterruptedException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        if (a == null) {
            request.setAttribute("arlet", "Bạn chưa đăng nhập");
            request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
        }
        String type = request.getParameter("network");
        int network = 0;
        if (type.equalsIgnoreCase("viettel")) {
            network = 1;
        } else if (type.equalsIgnoreCase("mobifone")) {
            network = 2;

        } else if (type.equalsIgnoreCase("vinafone")) {
            network = 3;

        } else if (type.equalsIgnoreCase("vietnamobile")) {
            network = 4;

        } else if (type.equalsIgnoreCase("gate")) {
            network = 5;

        } else if (type.equalsIgnoreCase("zing")) {
            network = 6;

        } else {
            network = 0;
        }
        if (network == 0) {
            request.setAttribute("arlet", "Không tồn tại nhà mạng này");
            request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
            return;
        }
//            String payment = request.getParameter("payment-method");

        String value = request.getParameter("numberquantitycard");
        String quantity = request.getParameter("amount");
        int total = Integer.parseInt(value) * Integer.parseInt(quantity);

        DAO dao = new DAO();
        DaoAccount daoAccount = new DaoAccount();
        Account ac = daoAccount.getUserbyId(a.getId());
        ArrayList<TransHis> transactionList = new ArrayList<TransHis>();
        int idTrans = dao.getTotalHis();
        dao.insertTransHis(a, total, 0, idTrans);

        if (ac.getMoney() - total < 0) {
            request.setAttribute("arlet", "Số dư không đủ");
            request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
        } else {
            Queue<Integer> queue = new LinkedList<>();
            queue.add(idTrans);
            while (!queue.isEmpty()) {
                idTrans = queue.remove();
                int money = daoAccount.getMoney(a.getUser());
                if (money < 0) {
                    request.setAttribute("arlet", "Số dư không đủ");
                    request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
                } else {
                    dao.updateTransHis(1, 1, idTrans);
                    daoAccount.subMoney(a, total);
                    a.setMoney(a.getMoney() - total);
                    dao.insertOrder(a, network, total, Integer.parseInt(quantity), Integer.parseInt(value), idTrans);
                }
            }

        }

        request.setAttribute("arlet", "Thành Công. Đang xử lý đơn hàng");
        session.getAttribute("acc");
        request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(AddOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(AddOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

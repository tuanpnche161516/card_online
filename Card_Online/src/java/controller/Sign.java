/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import model.DaoAccount;
import model.DaoToken;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import service.SendMailBySite;
import service.Util;

/**
 *
 * @author tuan
 */
@WebServlet(name = "sign", urlPatterns = {"/sign"})
public class Sign extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final int CAPTCHA_WIDTH = 200;
    private static final int CAPTCHA_HEIGHT = 60;
    private static final int CAPTCHA_LENGTH = 4;
    private static final String SESSION_CAPTCHA_ATTR = "captcha";
    public String captcha;
    private String name;
    private String pass;
    private String recapcha;
    String email;
    String imageAsBase64;
    BufferedImage image;
    String regexPattern = "[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("acc") != null) {
            response.sendRedirect("home");
        }
        response.setContentType("text/html;charset=UTF-8");
        name = request.getParameter("name");
        pass = request.getParameter("pass");
        email = request.getParameter("email");
        recapcha = request.getParameter("recaptcha");
        captcha = (String) session.getAttribute("captcha");
        Date javaDate = new java.util.Date();
        Timestamp timestamp = new java.sql.Timestamp(javaDate.getTime());
        DaoAccount daoAccount = new DaoAccount();
        DaoToken daoToken = new DaoToken();
        SendMailBySite m = new SendMailBySite();
        if (name != null && pass != null) {
            if (recapcha.equalsIgnoreCase(captcha)) {
                Account acc = daoAccount.checkUserbyName(name);
                if (acc == null) {
                    if (patternMatches(email, regexPattern)) {
                        daoAccount.insertUser(name, pass, email);
                        acc = daoAccount.checkUserbyName(name);
                        daoToken.updateToken(acc.getId());
                        session.setAttribute("AccountActive", acc);
                        m.sendMailActive(email, acc.getId());
                        response.sendRedirect(request.getContextPath() + "/active");
                        return;
                    } else {
                        request.setAttribute("arlet", "Định dạng email không chính xác");
                    }
                } else {
                    request.setAttribute("arlet", "Tài khoản đã tồn tại");
                }
            } else if (recapcha.length() != 0) {
                request.setAttribute("arlet", "Mã captcha của bạn không chính xác");
            }
        }
        request.setAttribute("name", name);
        request.setAttribute("pass", pass);
        request.setAttribute("email", email);
        request.getRequestDispatcher("sign.jsp").forward(request, response);
    }

    private boolean patternMatches(String emailAddress, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

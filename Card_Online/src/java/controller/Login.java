/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.DaoAccount;
import model.DaoToken;
import service.SendMailBySite;
import service.Util;

/**
 *
 * @author tuan
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    public String captcha;
    private String name;
    private String pass;
    private String recapcha;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DaoAccount dao = new DaoAccount();
        SendMailBySite m = new SendMailBySite();
        DaoToken daoToken = new DaoToken();
        if (session.getAttribute("acc") != null) {
            response.sendRedirect("home");
            return;
        }
        captcha = (String) session.getAttribute("captcha");
        name = request.getParameter("name");
        pass = request.getParameter("pass");
        recapcha = request.getParameter("recaptcha");
        if (name != null && pass != null && recapcha != null) {
            if (recapcha.equalsIgnoreCase(captcha)) {
                Account a = dao.getUser(name, pass);
                if (a != null) {
                    if (a.getIsActive() == -1) {
                        daoToken.updateToken(a.getId());
                        m.sendMailActive(a.getEmail(), a.getId());
                        session.removeAttribute("imageAsBase64");
                        session.setAttribute("AccountActive", a);
                        response.sendRedirect(request.getContextPath() + "/active");
//                        response.sendRedirect(request.getContextPath() + "/active?id=" + a.getId() + "&name=" + name);
//                        request.setAttribute("email", "Đã gửi về mail: " + a.getEmail());
//                        RequestDispatcher dispatcher = request.getRequestDispatcher(request.getContextPath() + "/active");
//                        dispatcher.forward(request, response);
                        return;
                    }
                    session.removeAttribute("imageAsBase64");
                    session.setAttribute("acc", a);
                    session.setMaxInactiveInterval(30 * 60 * 30); // 30p
                    response.sendRedirect("home");
                    return;
                } else {
                    request.setAttribute("name", name);
                    request.setAttribute("pass", pass);
                    request.setAttribute("arlet", "Tài khoản hoặc mật khẩu không chính xác");
                }
            } else if (recapcha.length() != 0) {
                System.out.println(recapcha.length());
                request.setAttribute("name", name);
                request.setAttribute("pass", pass);
                request.setAttribute("arlet", "Mã captcha sai");
            }
        }
        request.setAttribute("name", name);
        request.setAttribute("pass", pass);
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import entity.Token;
import model.DaoAccount;
import model.DaoToken;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author LEGION
 */
@WebServlet(name = "active", urlPatterns = {"/active"})
public class Active extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("AccountActive");

        if (a == null) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        if (request.getAttribute("email") == null) {
            request.setAttribute("email", a.getEmail());
        }
        String getToken = request.getParameter("otp");
        DaoToken daoToken = new DaoToken();
        Token token = daoToken.getToken(getToken);
        if (getToken != null) {
            if (token != null) {
                Timestamp now = new Timestamp(System.currentTimeMillis());
                if (token.getId_account() == a.getId() && token.getType() == 1 && token.getStatus() == 1) {
                    if (now.after(token.getDate())) {
                        System.out.println("Code hết hạn");
                    } else {
                        DaoAccount daoAccount = new DaoAccount();
                        daoAccount.updateActive(a.getId());
                        daoToken.updateTokenActive(token.getId());
                        session.removeAttribute("AccountActive");
                        response.sendRedirect(request.getContextPath() + "/login");
                        return;
                    }
                } else {
                    request.setAttribute("arlet", "Không tồn tại mã");
                }
            } else {
                request.setAttribute("arlet", "Không tồn tại mã");

            }
            request.setAttribute("email", null);
        }
        request.getRequestDispatcher("active-account.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

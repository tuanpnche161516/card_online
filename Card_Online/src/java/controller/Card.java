/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import entity.CategoryProduct;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.DaoProduct;

/**
 *
 * @author quang
 */
@WebServlet(name = "card", urlPatterns = {"/card"})
public class Card extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("acc") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        List<Account> listSession = new ArrayList<>();
        listSession.add((Account) session.getAttribute("acc"));

        DaoProduct a = new DaoProduct();
        List<CategoryProduct> listC = a.getCategory();
        String litmited = request.getParameter("litmit");
        String idPage = request.getParameter("page");
        int idPa = 0;
        if (idPage == null) {
            idPa = 1;
        } else {
            idPa = Integer.parseInt(idPage);
        }

        int litmit = 0;
        if (litmited == null) {
            litmit = 5;
        } else {
            litmit = Integer.parseInt(litmited);
        }
        List<Product> listP = a.pagingProduct(idPa, litmit);
        int count = a.getTotalProduct();
        int amountPage = count / litmit;
        
        if (count % litmit != 0) {
            amountPage++;
        }
        
        request.setAttribute("endPage", amountPage);
        request.setAttribute("listC", listC);
        request.setAttribute("listPP", listP);
        request.setAttribute("current", idPa);
        request.getRequestDispatcher("Card.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

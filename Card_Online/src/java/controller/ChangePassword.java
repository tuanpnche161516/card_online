/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import entity.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import model.DaoAccount;

/**
 *
 * @author MSI LAPTOP
 */
@WebServlet(name = "changepassword", urlPatterns = {"/changepassword"})
public class ChangePassword extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String newpass1 = request.getParameter("newpass1");
        String account = request.getParameter("account");
        DaoAccount a = new DaoAccount();
        if (oldpass != null && newpass != null && newpass1 != null) {
            if (!oldpass.equals(a.getPassByAccount(account))) {
                request.setAttribute("alert", "Nhập sai mật khẩu hiện tại");
                request.getRequestDispatcher("change_pass_profile.jsp").forward(request, response);
            } else if (oldpass.equals(newpass)) {
                request.setAttribute("alert", "Không đổi được thành mật khẩu hiện tại!");
                request.getRequestDispatcher("change_pass_profile.jsp").forward(request, response);
            } else if (!newpass.equals(newpass1)) {
                request.setAttribute("alert", "Nhập lại mật khẩu không khớp");
                request.getRequestDispatcher("change_pass_profile.jsp").forward(request, response);
            } else {

                a.updatePass(newpass, a.getIdByAccount(account));
                request.setAttribute("alert", "Đổi mật khẩu thành công");
                session.removeAttribute("acc");
                request.getRequestDispatcher(request.getContextPath()+"index.jsp").forward(request, response);
                //request.getRequestDispatcher("change_pass_profile.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("alert", oldpass + " " + newpass + " " + newpass1);
            request.getRequestDispatcher("change_pass_profile.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import entity.Account;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import model.DaoAccount;
import model.DaoProduct;

/**
 *
 * @author quang
 */
@WebServlet(name = "PagingManagerAccount", urlPatterns = {"/PagingManagerAccount"})
public class PagingManagerAccount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PagingManagerAccount</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PagingManagerAccount at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        int itemsPerPage = 10;
        int start = (currentPage - 1) * itemsPerPage;
        int end = start + itemsPerPage;
        DaoAccount daoAccount = new DaoAccount();
        List<Account> dataList = daoAccount.PagingAccount(currentPage, 5);
        System.out.println(dataList.toString());
        int totalItems = daoAccount.getTotalAccount();
        int totalPages = (int) Math.ceil((double) totalItems / 5);

        PrintWriter out = response.getWriter();
        out.println("<table class=\"table datatable\">");
        out.println("<thead>\n"
                + "                                            <tr>\n"
                + "                                                <th scope=\"col\">ID</th>\n"
                + "                                                <th scope=\"col\">Tài khoản</th>\n"
                + "                                                <th scope=\"col\">Mật Khẩu</th>\n"
                + "                                                <th scope=\"col\">Email</th>\n"
                + "                                                <th scope=\"col\">Quản Trị Viên</th>\n"
                + "                                                <th scope=\"col\">Kích hoạt</th>\n"
                + "                                                <th scope=\"col\">Ví</th>\n"
                + "                                                <th scope=\"col\">Quản lý</th>\n"
                + "                                            </tr>\n"
                + "                                        </thead>");
        for (Account data : dataList) {
            String admin;
            if (data.getIsAdmin() == 1) {
                admin = "Đang làm";
            } else {
                admin = "Không có";
            }
            String active;
            if (data.getIsActive() == 1) {
                active = "Đã kích hoạt";
            } else {
                active = "Chưa được";
            }
            double money = data.getMoney();

// Định dạng số tiền thành chuỗi với định dạng tiền tệ
            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("vi", "VN"));
            String formattedMoney = formatter.format(money);
            out.println("<tr>\n"
                    + "                                            <th scope=\"row\">" + data.getId() + "</th>\n"
                    + "                                            <td>" + data.getUser() + "</td>\n"
                    + "                                            <td>" + data.getPassword() + "</td>\n"
                    + "                                            <td>" + data.getUser() + "</td>\n"
                    + "                                            <td>" + admin + "</td>\n"
                    + "                                            <td>" + active + "</td>\n"
                    + "<td id=\"money-cell\">" + formattedMoney + "</td>"
                    + "<td><button><a href=\"deleteaccount?aid=" + data.getId() + "\">Xoá</a></button></td>\n"
//                    + "<td><button><a  href=\"loadaccount?aid=" + data.getId() + "\">Cập nhật</a></button><br>"
                    + "                                        </tr>");
        }
        out.println("</table>");
        out.println("</ul>");
        out.println("<ul class=\"pagination\">");
        for (int i = 1; i <= totalPages; i++) {
            if (i == currentPage) {
                out.println("<li class=\"page-item active\"><a href=\"#" + i + "\" onclick=\"setCurrentPage(" + i + ")\" class=\"page-link\">" + i + "</a></li>");
            } else {
                out.println("<li class=\"page-item \"><a href=\"#" + i + "\" onclick=\"setCurrentPage(" + i + ")\" class=\"page-link\">" + i + "</a></li>");
            }
        }
        out.println("</ul>");
        out.flush();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

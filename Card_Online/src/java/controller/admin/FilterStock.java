package controller.admin;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import entity.Stock;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.DAO;

@WebServlet(urlPatterns = {"/FilterStock"})
public class FilterStock extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int price = Integer.parseInt(request.getParameter("filterPrice"));
        int type = Integer.parseInt(request.getParameter("filterNetwork"));
        int current = Integer.parseInt(request.getParameter("currentPage"));
        int size = Integer.parseInt(request.getParameter("pageSizeSelect"));
        int statuscard = Integer.parseInt(request.getParameter("filterStatus"));
        DAO dao = new DAO();
        int page = (current - 1) * size;
        List<Stock> stk = null;
        stk = dao.getStockPagingAjax(size, page, price, type, statuscard);

        StringBuilder html = new StringBuilder();
        html.append("<table class=\"table\">");
        html.append("<thead>\n"
                + "                                    <tr>\n"
                + "                                        <th scope=\"col\" class=\"col-id\">ID</th>\n"
                + "                                        <th scope=\"col\" class=\"col-name\">Tên Thẻ</th>\n"
                + "                                        <th scope=\"col\" class=\"col-seri\">Seri Thẻ</th>\n"
                + "                                        <th scope=\"col\" class=\"col-code\">Mã Thẻ</th>                                          \n"
                + "                                        <th scope=\"col\" class=\"col-price\">Mệnh Giá</th>\n"
                + "                                        <th scope=\"col\" class=\"col-date\">Ngày Thêm</th>\n"
                + "                                        <th scope=\"col\" class=\"col-expiry\">Hạn Sử Dụng</th>\n"
                + "                                        <th scope=\"col\" class=\"col-expiry\">Trạng Thái</th>\n"
                + "                                        <th scope=\"col\" class=\"col-action\">Hành Động</th>\n"
                + "                                    </tr>\n"
                + "                                </thead>");
        html.append(" <tbody>");
        int i = 1;
        String status = "";
        for (Stock stock : stk) {
            if (stock.getStatus() == 1) {
                status = "Đã Bán";
            } else {
                status = "Chưa bán";
            }
            html.append("<tr>\n"
                    + "                                            <td class=\"cell col-id\">" + stock.getId() + "</td>\n"
                    + "                                            <td class=\"cell col-name\">" + stock.getName() + "</td>\n"
                    + "                                            <td class=\"cell col-seri\">" + stock.getSeri() + "</td>\n"
                    + "                                            <td class=\"cell col-code\">" + stock.getCode() + "</td>\n"
                    + "                                            <td class=\"cell col-price\">" + stock.getPrice() + "</td>\n"
                    + "                                            <td class=\"cell col-date\">" + stock.getDate() + "</td>\n"
                    + "                                            <td class=\"cell col-date\">" + stock.getExpiry() + "</td>\n"
                    + "                                            <td class=\"cell col-date\">" + status + "</td>\n"
                    + "                                            <td class=\"col-action\">\n"
                    + "                                                <button class=\"action-button\"><a href=\"loadStock?xid=" + stock.getId() + "\">Cập nhật</a></button>\n"
                    + "                                                <button class=\"action-button\"><a href=\"deleteStock?pid=" + stock.getId() + "\">Xoá</a></button>\n"
                    + "                                            </td>\n"
                    + "                                        </tr>");
            i++;
        }
        html.append("</tbody>\n");
        html.append("</table>");
        JsonObject json = new JsonObject();

        json.add("products", new Gson().toJsonTree(stk));
        json.addProperty("pagination", html.toString());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

}

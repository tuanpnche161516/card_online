/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import entity.OrderCard;
import entity.Stock;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.DAO;

/**
 *
 * @author quang
 */
@WebServlet(name = "orderadminajax", urlPatterns = {"/orderadminajax"})
public class OrderAdminAjax extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int price = Integer.parseInt(request.getParameter("filterPrice"));
        int type = Integer.parseInt(request.getParameter("filterNetwork"));
        int current = Integer.parseInt(request.getParameter("currentPage"));
        int size = Integer.parseInt(request.getParameter("pageSizeSelect"));
        int statuscard = Integer.parseInt(request.getParameter("filterStatus"));
        DAO dao = new DAO();
        int page = (current - 1) * size;
        List<OrderCard> stk = null;
        stk = dao.getOrderPaging(size, page, price, type, statuscard);

        StringBuilder html = new StringBuilder();
        html.append("<table class=\"table\">");
        html.append("<thead>\n"
                + "                                            <tr>\n"
                + "                                                <!--<th scope=\"col\" class=\"col-id\">id</th>-->\n"
                + "                                                <th scope=\"col\" class=\"col-id\">Tài Khoản Số</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Loại Thẻ</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Số Lượng</th>\n"
                + "                                                <th scope=\"col\" class=\"col-code\">Đơn Giá</th>\n"
                + "                                                <th scope=\"col\" class=\"col-status\">Tổng Số Tiền</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Trạng Thái</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Ngày Tạo</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Ngày Cập Nhật</th>\n"
                + "                                            </tr>\n"
                + "                                        </thead>");
        html.append(" <tbody>");
        int i = 1;
        String status = "";
        String typeCard = "";
        for (OrderCard stock : stk) {
            switch (stock.getStatus()) {
                case 0:
                    status = "Đang Xử Lý";
                    break;
                case 1:
                    status = "Thành Công";
                    break;
                case 2:
                    status = "Thất bại";
                    break;
                default:
                    break;
            }
            switch (stock.getTypeCard()) {
                case 0:
                    typeCard = "Đang Xử Lý";
                    break;
                case 1:
                    typeCard = "Viettel";
                    break;
                case 2:
                    typeCard = "MobiFone";
                    break;
                case 3:
                    typeCard = "Vinaphone";
                    break;
                case 4:
                    typeCard = "Vietnamobile";
                    break;
                case 5:
                    typeCard = "Gate";
                    break;
                case 6:
                    typeCard = "Zing";
                    break;
                default:
                    break;
            }
            html.append("<tr>\n"
                    + "                                                <td class=\"cell col-id\">" + stock.getIdAccount() + "</td> \n"
                    + "                                                <td class=\"cell col-amount\">" + typeCard + "</td>\n"
                    + "                                                <td class=\"cell col-amount\">" + stock.getValue() + "</td>\n"
                    + "                                                <td class=\"cell col-id\">" + stock.getAmount() + "</td>\n"
                    + "                                                <td class=\"cell col-id\">" + stock.getTotal() + "</td>\n"
                    + "                                                <td class=\"cell col-id\">" + status + "</td> \n"
                    + "                                                <td class=\"cell col-id\">" + stock.getCreatedAt() + "</td> \n"
                    + "                                                <td class=\"cell col-id\">" + stock.getUpdatedAt() + "</td> \n"
                    + "                                            </tr>");
            i++;
        }
        html.append("</tbody>\n");
        html.append("</table>");
        JsonObject json = new JsonObject();

        json.add("products", new Gson().toJsonTree(stk));
        json.addProperty("pagination", html.toString());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

}

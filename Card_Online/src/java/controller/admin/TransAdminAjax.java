/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import entity.OrderCard;
import entity.TransHis;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.DAO;

/**
 *
 * @author quang
 */
@WebServlet(name = "transadminajax", urlPatterns = {"/transadminajax"})
public class TransAdminAjax extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int price = Integer.parseInt(request.getParameter("filterPrice"));
        int type = Integer.parseInt(request.getParameter("filterNetwork"));
        int current = Integer.parseInt(request.getParameter("currentPage"));
        int size = Integer.parseInt(request.getParameter("pageSizeSelect"));
        int statuscard = Integer.parseInt(request.getParameter("filterStatus"));
        DAO dao = new DAO();
        int page = (current - 1) * size;
        List<TransHis> stk = null;
        stk = dao.getTransitionAdminPaging(size, page, price, type, statuscard);

        StringBuilder html = new StringBuilder();
        html.append("<table class=\"table\">");
        html.append("<thead>\n"
                + "                                            <tr>\n"
                + "                                                <!--<th scope=\"col\" class=\"col-id\">id</th>-->\n"
                + "                                                <th scope=\"col\" class=\"col-id\">Tài Khoản Số</th>\n"
                + "                                                <th scope=\"col\" class=\"col-id\">Ngày Tạo</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Gia</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Trạng Thái</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Loại</th>\n"
                + "                                                <th scope=\"col\" class=\"col-seri\">Ngày Cập Nhật</th>\n"
                + "                                            </tr>\n"
                + "                                        </thead>");
        html.append(" <tbody>");
        int i = 1;
        String status = "";
        String typeCard = "";
        for (TransHis stock : stk) {
            switch (stock.getStatus()) {
                case 0:
                    status = "Đang Xử Lý";
                    break;
                case 1:
                    status = "Thành Công";
                    break;
                case 2:
                    status = "Thất bại";
                    break;
                default:
                    break;
            }
            switch (stock.getType()) {
                case 0:
                    typeCard = "Cộng tiền";
                    break;
                case 1:
                    typeCard = "Trừ Tiền";
                    break;
                default:
                    typeCard = "Không rõ";
                    break;
            }
            html.append("<tr>\n"
                    + "                                                <td class=\"cell col-id\">" + stock.getId_account() + "</td> \n"
                    + "                                                <td class=\"cell col-amount\">" + stock.getCreatedAt() + "</td>\n"
                    + "                                                <td class=\"cell col-amount\">" + stock.getMoney() + "</td>\n"
                    + "                                                <td class=\"cell col-id\">" + status + "</td> \n"
                    + "                                                <td class=\"cell col-amount\">" + typeCard + "</td>\n"
                    + "                                                <td class=\"cell col-id\">" + stock.getUpdate_time() + "</td> \n"
                    + "                                            </tr>");
            i++;
        }
        html.append("</tbody>\n");
        html.append("</table>");
        JsonObject json = new JsonObject();

        json.add("products", new Gson().toJsonTree(stk));
        json.addProperty("pagination", html.toString());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

}

package controller.admin;

import entity.Stock;
import java.io.IOException;
import java.io.InputStream;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

@WebServlet("/ImportExcel")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
public class ImportExcel extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("typecard"));
        int price = Integer.parseInt(request.getParameter("valuecard"));
        Part filePart = request.getPart("file");
        InputStream fileContent = filePart.getInputStream();
        String name = "";
        switch (id) {
            case 1:
                name = "viettel";
                break;
            case 2:
                name = "mobifone";
                break;
            case 3:
                name = "vinaphone";
                break;
            case 4:
                name = "vietnamobile";
                break;
            case 5:
                name = "gate";
                break;
            case 6:
                name = "zing";
                break;    
            default:
                break;
        }

        DAO dao = new DAO();
        try {
            Workbook workbook = WorkbookFactory.create(fileContent);
            Sheet sheet = workbook.getSheetAt(0);
            int rowCount = 0;
            List<Stock> stk;
            stk = new ArrayList<>();
            for (Row row : sheet) {
                int columnCount = 0;
                Stock stock = new Stock();
                for (Cell cell : row) {
                    switch (columnCount) {
                        case 1:
                            // ô đầu tiên là seri
                            String seri = cell.getStringCellValue().trim();
                            if (seri == null) {
                                return;
                            }
                            stock.setSeri(seri);
                            break;
                        case 2:
                            String code = cell.getStringCellValue().trim();
                            if (code == null) {
                                return;
                            }
                            // ô thứ hai là code
                            stock.setCode(code);
                            break;
                        case 3:
                            // ô thứ ba là experied
                            String strDate = cell.getStringCellValue().trim();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            Date parsedDate = null;
                            try {
                                parsedDate = dateFormat.parse(strDate);
                            } catch (ParseException ex) {
                                Logger.getLogger(ImportExcel.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Timestamp timestamp = new Timestamp(parsedDate.getTime());
                            stock.setExpiry(timestamp);
                            break;
                        default:
                            break;
                    }
                    columnCount++;
                }
                stock.setName(name);
                stock.setId_product(id);
                stock.setPrice(price);
                stk.add(stock); // thêm đối tượng Stock mới vào danh sách stk
                rowCount++;
            }
            for (Stock stock : stk) {
                try {
                    dao.addStock(stock);
                } catch (ParseException ex) {
                    Logger.getLogger(ImportExcel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (InvalidFormatException | EncryptedDocumentException ex) {
            Logger.getLogger(ImportExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

        // process data here
        String jsonResponse = "{ \"status\": \"success\" }";
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);
    }
}

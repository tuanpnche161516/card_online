package controller;

import entity.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import model.DaoToken;

import service.SendMailBySite;

@WebServlet("/ResendEmailServlet")
public class ResendEmailServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public ResendEmailServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        // Lấy email của người dùng từ session
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("AccountActive");
        String email = a.getEmail();
        // Kiểm tra email có tồn tại không
        if (email == null || email.isEmpty()) {
            out.print("Không tìm thấy địa chỉ email của bạn");
        } else {
            // Gửi lại email
            // ... Đoạn mã Java để gửi email giống như trong câu trả lời trước ...
            SendMailBySite m = new SendMailBySite();
            DaoToken daoToken = new DaoToken();
            daoToken.updateToken(a.getId());
            m.sendMailActive(a.getEmail(), a.getId());
            
            // Gửi thông báo về cho trang gửi yêu cầu AJAX
            out.print("Đã gửi lại email xác thực cho " + email);

        }
    }
}

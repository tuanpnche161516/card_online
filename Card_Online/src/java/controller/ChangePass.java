/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Account;
import entity.Token;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import model.DaoAccount;
import model.DaoToken;

/**
 *
 * @author quang
 */
@WebServlet(name = "changepass", urlPatterns = {"/changepass"})
public class ChangePass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if ChangePass servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    HttpSession session = request.getSession();
    Timestamp now = new Timestamp(System.currentTimeMillis());
    Account a = (Account) session.getAttribute("AccountChangePass");
    String pass = request.getParameter("pass");
    String repass = request.getParameter("repass");
    String otp = request.getParameter("otp");
    if (a == null) {
        response.sendRedirect(request.getContextPath() + "/home");
        return;
    }

    // Server-side validation for pass and repass fields
    if (pass == null || pass.isEmpty()) {
        request.setAttribute("arlet", "Vui lòng nhập mật khẩu mới.");
        request.getRequestDispatcher("change-pass.jsp").forward(request, response);
        return;
    }

    if (!pass.equals(repass)) {
        request.setAttribute("arlet", "Mật khẩu không khớp.");
        request.getRequestDispatcher("change-pass.jsp").forward(request, response);
        return;
    }

    // Regular expression for password validation
    String passwordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";
    if (!pass.matches(passwordPattern)) {
        request.setAttribute("arlet", "Mật khẩu phải chứa ít nhất 8 ký tự, bao gồm chữ hoa, chữ thường và số.");
        request.getRequestDispatcher("change-pass.jsp").forward(request, response);
        return;
    }
        if (pass != null && repass != null && otp != null) {
            if (pass.equals(repass)) {
                DaoToken daoToken = new DaoToken();
                Token tk = daoToken.getToken(otp);
                if (tk != null) {
                    if (tk.getId_account() == a.getId() && tk.getType() == 2 && tk.getStatus() == 1) {
                        if (now.after(tk.getDate())) {
                            request.setAttribute("arlet", "Mã xác minh đã hết hạn, vui lòng thử lại !");
                        } else {
                            DaoAccount daoAccount = new DaoAccount();
                            daoAccount.updatePass(pass, a.getId());
                            request.setAttribute("arlet", "Thay đổi mật khẩu thành công");
                            response.sendRedirect(request.getContextPath() + "/login");
                            return;
                        }
                    } else {
                        request.setAttribute("arlet", "Mã xác nhận sai");
                    }

                } else {
                    request.setAttribute("arlet", "Mã xác nhận sai");
                }
            } else {
                request.setAttribute("arlet", "Mật khẩu không khớp");
            }
            response.sendRedirect("change-pass?arlet=Password changed successfully.");
        }
            request.getRequestDispatcher("change-pass.jsp").forward(request, response);


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if ChangePass servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if ChangePass servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Returns ChangePass short description of the servlet.
     *
     * @return ChangePass String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

<%-- 
    Document   : managerproduct
    Created on : Jun 28, 2023, 1:32:26 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Tables / Data - NiceAdmin Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/Stockcss.css"/>   
        <style>

            .table {
                font-family: Arial, sans-serif;
                font-size: 14px;
                border-collapse: collapse; /* Loại bỏ khoảng cách giữa các ô */
            }

            /* Đặt màu nền cho tiêu đề bảng */
            .table thead {
                background-color: #f2f2f2; /* Màu xám nhạt */
            }

            /* Căn giữa nội dung bên trong các ô */
            .table th,
            .table td {
                padding: 10px;
                text-align: center;
                border: 1px solid #0d6efd; /* Đặt viền màu xanh cho các ô */
            }

            /* Đặt màu chữ và màu nền cho các cột */
            .col-id,
            .col-typecard,
            .col-amount,
            .col-code,
            .col-total,
            .col-detail,
            .col-status,
            .col-seri {
                font-weight: bold;
                color: #333; /* Màu chữ đen */
                background-color: #fff; /* Màu nền trắng */
            }

            /* Đặt màu chữ và nền cho các cột chứa dữ liệu */
            .cell {
                color: #555; /* Màu chữ xám đậm */
                background-color: #f9f9f9; /* Màu nền xám nhạt */
            }

            /* Đặt màu chữ và biểu tượng cho trạng thái "Thành Công" */
            .col-status .success {
                color: #228B22; /* Màu xanh lá cây */
            }

            .col-status .success-icon:before {
                content: "\2714"; /* Biểu tượng tick ✔ */
            }

            /* Đặt màu chữ và biểu tượng cho trạng thái "Thất Bại" */
            .col-status .failure {
                color: #B22222; /* Màu đỏ sẫm */
            }

            .col-status .failure-icon:before {
                content: "\2718"; /* Biểu tượng cross ✘ */
            }

            /* Tạo hiệu ứng khi di chuột vào hàng */
            .table tbody tr:hover {
                background-color: #f2f2f2; /* Màu xám nhạt */
            }
 
            /* Button styles for pagination */
            #next-page,
            #prev-page,
            #clear-filters {
                padding: 8px 16px;
                background-color: #007bff;
                border: none;
                color: #fff;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
                margin: 0 5px;
            }

            #next-page:hover,
            #prev-page:hover,
            #clear-filters:hover {
                background-color: #0056b3;
            }

        </style>

    </head>

    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
        <main id="main" class="main">
                <section class="section">
                    <div class="container">
                        <div class="card">
                            <div>
                                <label for="filter-price">Lọc sản phẩm theo giá trị:</label>
                                <select id="filter-price">
                                    <option value="0">Tất cả</option>
                                    <option value="10000">10,000 đ</option>
                                    <option value="20000">20,000 đ</option>
                                    <option value="50000">50,000 đ</option>
                                    <option value="100000">100,000 đ</option>
                                    <option value="200000">200,000 đ</option>
                                    <option value="500000">500,000 đ</option>
                                </select>
                            </div>
                            <div>
                                <label for="filter-network">Lọc sản phẩm theo nhà mạng:</label>
                                <select id="filter-network">
                                    <option value="0">Tất cả</option>
                                    <option value="1">Viettel</option>
                                    <option value="2">MobiFone</option>
                                    <option value="3">VinaPhone</option>
                                    <option value="4">Vietnamobile</option>
                                    <option value="5">Gate</option>
                                    <option value="6">Zing</option>
                                </select>
                            </div>
                            <div>
                                <label for="filter-status">Lọc sản phẩm đã dùng:</label>
                                <select id="filter-status">
                                    <option value="0">Tất Cả</option>
                                    <option value="2">Thành Công</option>
                                    <option value="1">Thất Bại</option>
                                </select>
                            </div>
                            <div>
                                <label for="page-size-select">Số sản phẩm trên trang:</label>
                                <select id="page-size-select">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                </select>
                            </div>

                            <div class="card-body">                                  
                                <table class="table">

                                    <tbody>
                                    <div id="pagination" class="pagination"></div>
                                    <div class="page" style="display: flex;">
                                    <div>
                                        <button id="next-page">Trang tiếp theo</button>
                                    </div>
                                    <div>
                                        <button id="prev-page">Trang trước</button>
                                    </div>
                                        </div>
                                    </tbody>
                                </table>                               
                            </div>
                        </div>
                    </div>
                </section>
        </main><!-- End #main -->

        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            $(document).ready(function () {
                var currentPage = 1;
                $('#filter-price, #filter-network, #filter-status, #page-size-select').on('change', function () {
                    var filterPrice = $('#filter-price').val();
                    var filterNetwork = $('#filter-network').val();
                    var filterStatus = $('#filter-status').val();
                    var pageSizeSelect = $('#page-size-select').val();
                    loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                });
                loadProductList('0', '0', currentPage, '-1', '5');
                $('#next-page').on('click', function () {
                    currentPage++; // Tăng giá trị của currentPage lên 1
                    var filterPrice = $('#filter-price').val();
                    var filterNetwork = $('#filter-network').val();
                    var filterStatus = $('#filter-status').val();
                    var pageSizeSelect = $('#page-size-select').val();
                    loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                });
                $('#prev-page').on('click', function () {
                    if (currentPage > 1) {
                        currentPage--; // Giảm giá trị của currentPage đi 1
                        var filterPrice = $('#filter-price').val();
                        var filterNetwork = $('#filter-network').val();
                        var filterStatus = $('#filter-status').val();
                        var pageSizeSelect = $('#page-size-select').val();
                        loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                    }
                });
                $('#clear-filters').on('click', function () {
                    // Đặt giá trị của các phần tử filter về giá trị mặc định
                    $('#filter-price').val('0');
                    $('#filter-network').val('0');
                    $('#filter-status').val('-1');
                    $('#page-size-select').val('5');

                    // Tải lại danh sách sản phẩm với filter mặc định
                    loadProductList('0', '0', currentPage, '-1', '5');
                });
            });
            function loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect) {
                $.ajax({
                    url: 'orderadminajax',
                    type: 'GET',
                    data: {
                        filterPrice: filterPrice,
                        filterNetwork: filterNetwork,
                        currentPage: currentPage,
                        filterStatus: filterStatus,
                        pageSizeSelect: pageSizeSelect
                    },
                    success: function (data) {
                        $('#product-list').html(data.products);
                        $('#pagination').html(data.pagination);
                        $('.page-link').on('click', function () {
                            var page = $(this).data('page');
                            loadProductList(filterPrice, filterNetwork, page, filterStatus, pageSizeSelect);
                        });
                    }
                });
            }
        </script>
    </body>

</html>
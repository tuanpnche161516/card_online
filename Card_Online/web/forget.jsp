<%-- 
    Document   : login
    Created on : Jun 26, 2023, 11:41:20 PM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Quên mật khẩu</title>
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <main>
             <c:if test="${arlet !=null}">
                    <div class="alert">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <strong>Thông Báo!</strong> ${arlet}
                    </div>
                </c:if>
            <div class="container">
               
                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-6 d-flex flex-column align-items-center justify-content-center">
                                <div class="d-flex justify-content-center py-4">
                                    <a href="" class="logo d-flex align-items-center w-auto">
                                        <img src="assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">Card Online</span>
                                    </a>
                                </div><!-- End Logo -->

                                <div class="card mb-3">
                                    <div class="card-body">
                                        <div class="pt-4 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Quên mật khẩu</h5>
                                            <p class="text-center small">Nhập tài khoản của bạn</p>
                                        </div>
                                        <form class="row g-4 needs-validation" action="forget" method="get">
                                            <div class="col-12">
                                                <label class="form-label">Tài khoản</label>
                                                <div class="input-group ">
                                                    <input type="text" name="name" value="${name}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Captcha</label>
                                                <div>
                                                    <img id="captchaImage" src="CaptchaServlet" alt="Captcha Image">
                                                    <button  type="reset" id="refreshCaptcha">Tạo mới</button>
                                                </div>
                                                <input type="text" name="recaptcha" class="form-control">
                                            </div>
                                            <div class="col-12">
                                                <button class="btn btn-primary w-100" type="submit">Quên mật khẩu</button>
                                            </div>

                                        </form>
                                        <div class="col-md-6">
                                            <p class="small mb-2">Bạn chưa có tài khoản? <a href="sign">Đăng ki</a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="small mb-2">Bạn có tài khoản rồi? <a href="login">Đăng nhập</a></p>
                                        </div>
                                        <div class="col-12">
                                            <p class="small mb-2"><a href="login">Quay lại</a></p>
                                        </div>
<!--                                        <img id="captchaImage" src="${test}" alt="Captcha Image" />
                                        <br />
                                        <button onclick="refreshCaptcha()">Làm mới Captcha</button>-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </main><!-- End #main -->
        <script>
            document.getElementById("refreshCaptcha").addEventListener("click", function () {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            document.getElementById("captchaImage").src = "CaptchaServlet?" + new Date().getTime();
                        } else {
                            console.log("Lỗi khi tạo lại mã Captcha");
                        }
                    }
                };
                xhr.open("GET", "CaptchaServlet?refresh=" + new Date().getTime(), true);
                xhr.send();
            });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="assets/js/main1.js"></script>
    </body>
</html>
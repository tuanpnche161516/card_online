
function highlightSelected(event, optionId) {
    event.preventDefault(); // Ngăn chặn hành vi mặc định của thẻ <a>

    // Xóa lớp selected từ tất cả các phần tử option
    var options = document.getElementsByClassName("option");
    for (var i = 0; i < options.length; i++) {
        options[i].classList.remove("selected");
    }

    // Thêm lớp selected cho option được chọn
    var selectedOption = event.target.closest(".option");
    selectedOption.classList.add("selected");




}
//  function highlightSelected(event, optionId) {
//  event.preventDefault(); // Ngăn chặn hành vi mặc định của thẻ <a>
//
//  // Xóa lớp selected từ tất cả các phần tử option
//  var options = document.getElementsByClassName("option");
//  for (var i = 0; i < options.length; i++) {
//    options[i].classList.remove("selected");
//  }
//
//  // Thêm lớp selected cho option được chọn
//  var selectedOption = event.target.closest(".option");
//  selectedOption.parentElement.classList.add("selected");
//}

function showPrices(option) {
    var priceOptions = document.getElementById("price-options");

    if (option === 'option1') {
        // Hiển thị mệnh giá cho option 1 (Viettel)
        priceOptions.style.display = "block";
    } else if (option === 'option2') {
        // Hiển thị mệnh giá cho option 2 (Mobifone)
        priceOptions.style.display = "block";
    } else if (option === 'option3') {
        // Hiển thị mệnh giá cho option 3 (VinaPhone)
        priceOptions.style.display = "block";
    } else if (option === 'option4') {
        // Hiển thị mệnh giá cho option 4 (Gate)
        priceOptions.style.display = "block";
    } else if (option === 'option5') {
        // Hiển thị mệnh giá cho option 5 (Garena)
        priceOptions.style.display = "block";
    }
}

//function addToCart() {
//  var selectedPrice = document.querySelector('input[name="price"]:checked');
//  var quantity = selectedPrice.parentNode.querySelector('input[name="quantity"]').value;
//  
//  if (selectedPrice && quantity > 0) {
//    var cartItems = document.getElementById("cart-items");
//    var cartItem = document.createElement("div");
//    cartItem.textContent = "Đã thêm " + quantity +" thẻ vào giỏ hàng";
//    cartItems.appendChild(cartItem);
//    
//    // Reset mức giá và số lượng sau khi thêm vào giỏ hàng
//    selectedPrice.checked = false;
//    selectedPrice.parentNode.querySelector('input[name="quantity"]').value = 0;
//    
//    // Ẩn các mệnh giá sau khi thêm vào giỏ hàng
//    var priceOptions = document.getElementById("price-options");
//    priceOptions.style.display = "none";
//   
//  }
//}

function addToCart() {
    var selectedPrice = document.querySelector('input[name="price"]:checked');
    var quantity = selectedPrice.parentNode.querySelector('input[name="quantity"]').value;

    if (selectedPrice && quantity > 0) {
        var cartItems = document.getElementById("cart-items");
        var cartItem = document.createElement("div");
        cartItem.textContent = "Đã thêm " + quantity + " thẻ vào giỏ hàng";
        cartItems.appendChild(cartItem);

        // Reset mức giá và số lượng sau khi thêm vào giỏ hàng
        selectedPrice.checked = false;
        selectedPrice.parentNode.querySelector('input[name="quantity"]').value = 0;

        // Ẩn các mệnh giá sau khi thêm vào giỏ hàng
        var priceOptions = document.getElementById("price-options");
        priceOptions.style.display = "none";

        // Xóa thông báo sau 2 giây
        setTimeout(function () {
            cartItems.removeChild(cartItem);
        }, 2000);
    }
}
function showQuantityInput(quantityId) {
    var quantityInputs = document.querySelectorAll('input[type="number"]');
    quantityInputs.forEach(function (input) {
        input.style.display = "none";
    });
    var selectedQuantityInput = document.getElementById(quantityId);
    selectedQuantityInput.style.display = "inline-block";
}







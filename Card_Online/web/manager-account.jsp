<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Tables / Data - NiceAdmin Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">

    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
        <main id="main" class="main">
            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Tài Khoản</h5>
                                <!-- Table with stripped rows -->
                                <div id="dataList"></div>
                                <ul class="pagination">
                                    <div id="pagination"></div>
                                </ul>
                            </div>	
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->
        <!-- Thẻ divđể hiển thị danh sách các mục -->

        <!-- Thẻ div để hiển thị các liên kết phân trang -->

        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>
        <script>
            let moneyElement = document.getElementById('money-cell');
            let money = parseFloat(moneyElement.textContent);
            let formattedMoney = money.toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
            moneyElement.textContent = formattedMoney;
        </script>
        <script>
            var currentPage = 1;
            var itemsPerPage = 10;

            // Hàm để lấy danh sách các mục từ servlet và cập nhật nội dung của thẻ div "dataList"
            function getDataList() {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", "PagingManagerAccount?currentPage=" + currentPage + "&itemsPerPage=" + itemsPerPage, true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        var dataListDiv = document.getElementById("dataList");
                        dataListDiv.innerHTML = xhr.responseText;
                    }
                };
                xhr.send();
            }

            // Hàm để cập nhật trang hiện tại và gọi hàm getDataList() để lấy danh sách các mục mới
            function setCurrentPage(page) {
                currentPage = page;
                getDataList();
            }

            // Gọi hàm getDataList() lần đầu để lấy danh sách các mục ban đầu
            getDataList();
        </script> 
    </body>
</html>
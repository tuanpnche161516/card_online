<%-- 
    Document   : cart
    Created on : Jul 6, 2023, 8:35:04 PM
    Author     : MSI LAPTOP
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Dashboard</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">

        <!-- =======================================================
        * Template Name: NiceAdmin
        * Updated: May 30 2023 with Bootstrap v5.3.0
        * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
        
    </head>
    <body>
               <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <!-- End Sidebar-->

            <main id="main" class="main">

                <div class="pagetitle">
                    <h1></h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Thẻ Online</li>
                        </ol>
                    </nav>
                </div><!-- End Page Title -->
                
                <section class="section dashboard">
                    <strong>${alert}</strong>
                    <div class="row">

                        <table>
                            <tr>
                                <td>Tên sản phảm</td>
                                <td>Giá</td>
                                <td>Số lượng</td>
                                <td></td>
                                <td></td>
                            </tr>
                            
                            <c:forEach items="${data}" var="i">
                                <tr>
                                    <td>${i.getName()}</td>
                                    <td>${i.getPrice()}</td>
                                    <form action="pay">
                                    <td>
                                            <input type="number" name="quantity" id="quantity" min="1" max="100">
                                            <input type="text" name="acc" value="${sessionScope.acc.user}" style="display: none">
                                            <input type="text" name="id" value="${i.getId()}" style="display: none">
                                    </td>
                                    <td><input type="submit" name="submit" value="Mua"></td>
                                    <td><input type="submit" name="submit" value="Xóa"></td>
                                    </form>
                                </tr>
                            </c:forEach>
                            
                        </table>
                        <!-- Left side columns -->
                        <div class="col-lg-8">
                            <div class="row">

                                <!-- Sales Card -->
                                
                                <div class="col-xxl-4 col-md-6">
                                    
                                </div><!-- End Sales Card -->
                            </div>
                        </div><!-- End Left side columns -->

                        <!-- Right side columns -->
                        <div class="col-lg-4">


                        </div><!-- End Right side columns -->

                    </div>
                </section>

            </main><!-- End #main -->

            <!-- ======= Footer ======= -->
        <jsp:include page="end.jsp"></jsp:include>

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>s
        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>

    </body>
</html>

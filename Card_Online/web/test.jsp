<%-- 
    Document   : test
    Created on : Jun 29, 2023, 11:49:33 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Card Quantity</title>
        <style>
            .alert {
                padding: 20px;
                background-color: #f44336;
                color: white;
            }

            .success {
                background-color: #4CAF50;
            }

            .error {
                background-color: #f44336;
            }
        </style>
    </head>
    <body>
            
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
            <script>
                $(document).ready(function () {
                    var currentPage = 1;
                    $('#filter-price, #filter-network, #filter-status, #page-size-select').on('change', function () {
                        var filterPrice = $('#filter-price').val();
                        var filterNetwork = $('#filter-network').val();
                        var filterStatus = $('#filter-status').val();
                        var pageSizeSelect = $('#page-size-select').val();
                        loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                    });
                    loadProductList('0', '0', currentPage, '-1', '5');
                    $('#next-page').on('click', function () {
                        currentPage++; // Tăng giá trị của currentPage lên 1
                        var filterPrice = $('#filter-price').val();
                        var filterNetwork = $('#filter-network').val();
                        var filterStatus = $('#filter-status').val();
                        var pageSizeSelect = $('#page-size-select').val();
                        loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                    });
                    $('#prev-page').on('click', function () {
                        if (currentPage > 1) {
                            currentPage--; // Giảm giá trị của currentPage đi 1
                            var filterPrice = $('#filter-price').val();
                            var filterNetwork = $('#filter-network').val();
                            var filterStatus = $('#filter-status').val();
                            var pageSizeSelect = $('#page-size-select').val();
                            loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                        }
                    });
                });
                function loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect) {
                    $.ajax({
                        url: 'FilterProductServlet',
                        type: 'GET',
                        data: {
                            filterPrice: filterPrice,
                            filterNetwork: filterNetwork,
                            currentPage: currentPage,
                            filterStatus: filterStatus,
                            pageSizeSelect: pageSizeSelect
                        },
                        success: function (data) {
                            $('#product-list').html(data.products);
                            $('#pagination').html(data.pagination);
                            $('.page-link').on('click', function () {
                                var page = $(this).data('page');
                                loadProductList(filterPrice, filterNetwork, page, filterStatus, pageSizeSelect);
                            });
                        }
                    });
                }
            </script>
    </body>
</html>
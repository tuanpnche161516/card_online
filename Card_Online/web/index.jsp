<%-- 
    Document   : index
    Created on : Jun 26, 2023, 11:17:31 PM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Dashboard</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css"/>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.network-option, .amount-option').on('click', function () {
                    // Lấy giá trị nhà mạng và mệnh giá được chọn
                    var network = $('.network-option:checked').val();
                    var amount = $('.amount-option:checked').val();

                    // Gửi yêu cầu AJAX để lấy số lượng thẻ còn lại
                    $.ajax({
                        type: 'GET',
                        url: '/card_quantity',
                        data: {network: network, amount: amount},
                        dataType: 'json',
                        success: function (data) {
                            // Cập nhật số lượng thẻ còn lại trên trang web
                            $('#quantity').text(data.quantity);
                        },
                        error: function () {
                            console.log('Error getting card quantity.');
                        }
                    });
                });
            });
        </script>
        <style>
            .amount-label {
                position: relative;
                display: block;
                height: 100%;
            }

            .amount-option {
                position: absolute;
                opacity: 0;
                width: 100%;
                height: 100%;
                cursor: pointer;
            }

            .amount-label:before {
                content: '';
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                border-radius: 5px;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            .amount-option:checked + .amount-label:before {
                background-color: black;
                color: white;
                box-shadow: 0 0 5px #007bff;
            }

            .amount-label span {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 18px;
                height: 100%;
            }

            .amount-value {
                font-size: 14px;
            }

            .card {
                height: 100px;
                width: 100%;
                box-sizing: border-box;
                border-radius: 5px;
                border: 1px solid #ccc;
            }
            .card img {
                max-height: 100%;
                max-width: 100%;
                display: block;
                margin: auto;
            }
            .network-option {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

        </style>
    </head>

    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <!-- End Sidebar-->


            <main id="main" class="main">

                <div class="pagetitle">
                    <h1></h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Thẻ Online</li>
                        </ol>
                    </nav>
                </div><!-- End Page Title -->

                <section class="section dashboard">
                    <!-- Left side columns -->
                <c:if test="${alert !=  null}">
                    <div class="alert" style="background-color: green;">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <strong>Thông Báo!</strong> ${alert}
                    </div>
                </c:if>
                <form method="post" action="addorder">
                    <div class="col-lg-8 " style="float: left;">
                        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                            <div class="group-header" style="background-color: #f5f5f5; font-weight: bold; color: black; height: 100%; padding: 10px">Chọn Loại Thẻ</div>
                        </div>
                        <div class="row">
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/vt.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Viettel">
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/mobi.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Mobifone">
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/vina.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Vinaphone">
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/vnmb.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Vietnamobile">
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/GATE_01.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Gate">
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <img src="img/ZING_01.png" alt="alt"/>
                                        <input type="radio" name="network" class="network-option" value="Zing">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                            <div class="group-header" style="background-color: #f5f5f5; font-weight: bold; color: black; padding: 10px;">Chọn mệnh giá &amp; số lượng</div>

                        </div>
                        <div class="row">
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="10000">
                                        <span class="amount-value">10,000 đ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="20000">
                                        <span class="amount-value">20,000 đ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="50000">
                                        <span class="amount-value">50,000 đ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="100000">
                                        <span class="amount-value">100,000 đ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="200000">
                                        <span class="amount-value">200,000 đ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-md-4">
                                <div class="card info-card sales-card">
                                    <label class="amount-label">
                                        <input type="radio" name="amount" class="amount-option" value="500000">
                                        <span class="amount-value">500,000 đ</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 row">
                            <div class="form-group">
                                <label for="quantity">Số lượng:</label>
                                <input type="number" name="numberquantitycard" id="numberquantity" class="form-control" value="1" min="1" max="10">
                            </div>
                            <label>Còn Lại: <label id="quantity">0</label></label>
                        </div>


                        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                            <div class="group-header" style="background-color: #f5f5f5; font-weight: bold; color: black; padding: 10px;">Phương Thức Thanh Toán</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="radio-group">
                                    <input type="radio" id="wallet-radio" name="payment-method" value="wallet" checked>
                                    <label for="wallet-radio">Thông qua Ví</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="radio-group">
                                    <input type="radio" id="wallet-radio" name="payment-method" value="wallet">
                                    <label for="wallet-radio">Thông qua Ví Điện Tử</label>
                                </div>
                            </div>
                        </div>
                    </div>  
                   
                    <div class="col-lg-4" style="float: right;">
                        <div class="card" style="height: 100%; margin-top: 5%">
                            <div class="card-body">
                                <h5 class="card-title" style="  font-weight: bold;">Thanh toán</h5>
                                <div class="container">
                                    <div class="row">
                                        <div class="group-title" style="width: 100%; font-size: 16px;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="group-header light">CHI TIẾT GIAO DỊCH</div>
                                    </div>
                                    <div class="row group-content">
                                        <div class="row content-row no-border">
                                            <div class="custome-col col col-6">Loại mã thẻ</div>
                                            <div class="custome-col col col-6">
                                                <div class="custome-col col col-6">
                                                    <div class="quantity-panel info-price big" id="type">Chọn loại thẻ</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row content-row no-border">
                                            <div class="custome-col col col-6">Mệnh giá thẻ</div>
                                            <div class="custome-col col col-6">
                                                <div class="custome-col col col-6">
                                                    <div class="quantity-panel info-price big" id="price"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Số lượng</div>
                                            <div class="custome-col col col-6">
                                                <div class="custome-col col col-6"><div class="quantity-panel info-price big" id="amountcard">1</div></div>
                                            </div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Email nhận</div>
                                            <div class="custome-col col col-6"><div class="quantity-panel info-price">${sessionScope.acc.email}</div></div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Phí giao dịch</div>
                                            <div class="custome-col col col-6"><div class="quantity-panel info-price">Miễn phí</div></div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Giảm giá</div>
                                            <div class="custome-col col col-6"><div class="quantity-panel info-price">0 đ</div></div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Phương thức</div>
                                            <div class="custome-col col col-6">
                                                <div class="quantity-panel info-price" id="selected-payment-method">Chưa chọn</div>
                                            </div>
                                        </div>
                                        <div class="row content-row">
                                            <div class="custome-col col col-6">Tổng tiền:</div>
                                            <div class="custome-col col col-6">
                                                <div class="quantity-panel info-price big" id="total-price">0 đ</div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="pay-button">Thanh toán</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </section>
        </main><!-- End #main -->


        <script>
// Lấy các phần tử input có class là "network-option"
            var networkInputs = document.querySelectorAll('.network-option');

// Lấy phần tử HTML có id là "type"
            var typePanel = document.getElementById('type');

// Lấy các phần tử input có class là "amount-option"
            var amountInputs = document.querySelectorAll('.amount-option');

// Lấy phần tử HTML có id là "price"
            var pricePanel = document.getElementById('price');

// Lấy phần tử input có id là "numberquantity"
            var quantityInput = document.getElementById('numberquantity');

// Lấy phần tử HTML có id là "amount"
            var amountPanel = document.getElementById('amountcard');

// Gán sự kiện "change" cho từng phần tử input mạng để lắng nghe sự thay đổi
            networkInputs.forEach(function (networkInput) {
                networkInput.addEventListener('change', function () {
                    // Lấy giá trị của thuộc tính "value" của phần tử input
                    var networkValue = networkInput.value;

                    // Hiển thị giá trị vào phần tử HTML có id là "type"
                    typePanel.innerHTML = networkValue;
                });
            });

// Gán sự kiện "change" cho từng phần tử input số tiền để lắng nghe sự thay đổi
            amountInputs.forEach(function (amountInput) {
                amountInput.addEventListener('change', function () {
                    // Lấy giá trị của thuộc tính "value" của phần tử input
                    var amountValue = amountInput.value;

                    // Hiển thị giá trị vào phần tử HTML có id là "price"
                    var formattedAmount = parseInt(amountValue).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
                    pricePanel.innerHTML = formattedAmount;
                });
            });

// Gán sự kiện "change" cho phần tử input số lượng để lắng nghe sự thay đổi
            quantityInput.addEventListener('change', function () {
                // Lấy giá trị của thuộc tính "value" của phần tử input
                var quantityValue = quantityInput.value;

                // Hiển thị giá trị vào phần tử HTML có id là "amount"
                amountPanel.innerHTML = quantityValue;
            });
// Lấy phần tử HTML có id là "total-price"
            var totalPricePanel = document.getElementById('total-price');

// Tính toán tổng tiền và hiển thị giá trị mới trong phần tử HTML "total-price"
            function updateTotalPrice() {
                // Lấy giá trị của phần tử input số tiền được chọn
                var amountInputValue = document.querySelector('.amount-option:checked').value;

                // Lấy giá trị của phần tử input số lượng
                var quantityInputValue = document.getElementById('numberquantity').value;

                // Tính toán tổng tiền
                var totalPrice = parseInt(amountInputValue) * parseInt(quantityInputValue);

                // Hiển thị giá trị tổng tiền với định dạng tiền tệ
                var formattedTotalPrice = totalPrice.toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
                totalPricePanel.innerHTML = formattedTotalPrice;
            }

// Gọi hàm updateTotalPrice() khi giá trị các phần tử input thay đổi
            networkInputs.forEach(function (networkInput) {
                networkInput.addEventListener('change', updateTotalPrice);
            });

            amountInputs.forEach(function (amountInput) {
                amountInput.addEventListener('change', updateTotalPrice);
            });

            quantityInput.addEventListener('change', updateTotalPrice);
            
            
        </script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/main1.js"></script>
        <script src="assets/js/Highlight.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    </body>

</html>
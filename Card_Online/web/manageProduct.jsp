<%-- 
    Document   : managerproduct
    Created on : Jun 28, 2023, 1:32:26 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Tables / Data - NiceAdmin Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/Stockcss.css"/>
        <style>
            .action-button {
                display: inline-block;
                padding: 5px 10px;
                background-color: #5cb85c;
                color: #fff;
                text-decoration: none;
                border-radius: 4px;
                transition: background-color 0.3s;
                margin-right: 5px;
            }

            .action-button:hover {
                background-color: #449d44;
            }</style>
    </head>

    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <main id="main" class="main">
                <form action="manageProduct" method="post">
                    <section class="section">
                        <div class="container">
                            <div class="card">
                                <div class="card-body">
                                    <div class="header-buttons">
                                        <a href="addproduct" class="button button-primary">Thêm Sản Phẩm</a>       
                                        <div class="total">
                                            <h4>Sản Phẩm Trong Kho: ${count}</h4>                                     
                                    </div>  
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="col-id">ID</th>
                                            <th scope="col" class="col-img">Ảnh</th>
                                            <th scope="col" class="col-name">Tên Thẻ</th>
                                            <th scope="col" class="col-action">Hành Động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${detail}" var="o">
                                            <tr>
                                                <td class="cell col-id">${o.id}</td>
                                                <td class="cell col-img">${o.img}</td>
                                                <td class="cell col-name">${o.name}</td>                                                                                               
                                                <td class="col-action">
                                                    <button class="action-button"><a href="loadproduct?pid=${o.id}">Cập nhật</a></button>
                                                    <button class="action-button"><a href="deleteproduct?pid=${o.id}">Xoá</a></button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <ul class="pagination">
                                    <c:if test="${current > 1}">
                                        <li class="page-item disabled"><a href="managerproduct?page=${current-1}">Previous</a></li>
                                        </c:if>
                                        <c:forEach begin="1" end="${endPage}" var="i">
                                        <li class="page-item ${current == i ? 'active' : ''}">
                                            <a href="managerproduct?page=${i}" class="page-link">${i}</a>
                                        </li>
                                    </c:forEach>
                                    <c:if test="${current < endPage}">
                                        <li class="page-item"><a href="managerproduct?page=${current+1}" class="page-link">Next</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </main><!-- End #main -->
        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </footer><!-- End Footer -->
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    </body>

</html>
<%-- 
    Document   : active-account
    Created on : Jun 27, 2023, 9:02:06 AM
    Author     : quang
--%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Kích hoạt tài khoản</title>
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script>
            $(document).ready(function () {
                // Bắt sự kiện click của thẻ a
                $('#resendEmailLink').click(function (event) {
                    event.preventDefault(); // Ngăn chặn hành động mặc định của thẻ a

                    // Gửi yêu cầu AJAX để gửi lại email
                    $.ajax({
                        url: 'ResendEmailServlet',
                        method: 'POST',
                        success: function (response) {
                            // Hiển thị thông báo trả về từ server
                            alert(response);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // Xử lý lỗi
                            console.log('Lỗi: ' + textStatus);
                        }
                    });
                });
            });
        </script>
    </head>

    <body>

        <main>
            <div class="container">
                <c:if test="${arlet !=  null}">
                    <div class="alert">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <strong>Thông Báo!</strong> ${arlet}
                    </div>
                </c:if>
                <c:if test="${email !=  null}">
                    <div class="alertgreen">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <strong>Thông Báo!</strong>Đã gửi về mail: ${email}
                    </div>
                </c:if>
                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-6 d-flex flex-column align-items-center justify-content-center">
                                <div class="d-flex justify-content-center py-4">
                                    <a href="" class="logo d-flex align-items-center w-auto">
                                        <img src="assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">Card Online</span>
                                    </a>
                                </div><!-- End Logo -->
                                <div class="card mb-3">
                                    <div class="card-body">

                                        <div class="pt-4 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Nhập mã kích hoạt</h5>
                                            <p class="text-center small">Nhập tài khoản và mật khẩu</p>
                                        </div>

                                        <form class="row g-3 needs-validation" action="active" method="post">
                                            <div class="col-12">
                                                <label class="form-label">Mã kích hoạt</label>
                                                <div class="input-group ">
                                                    <input type="text" name="otp" value="" class="form-control">
                                                </div>
                                                <div class="col-6">
                                                    <a href="#" id="resendEmailLink">Gửi lại email</a>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button class="btn btn-primary w-100" type="submit">Đồng ý</button>
                                            </div>
                                        </form>
                                        <div class="col-12">
                                            <p class="small mb-2"><a href="sign">Quay lại</a></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </main><!-- End #main -->


        <script src="assets/js/main1.js"></script>
    </body>

</html>
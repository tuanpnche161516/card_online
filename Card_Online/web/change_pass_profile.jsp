<%-- 
    Document   : profile
    Created on : Jun 29, 2023, 10:39:09 AM
    Author     : quang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Thong tin cá nhân</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <style>
            .button-group {
                text-align: center;
            }

            .button-group button,
            .button-group a {
                display: inline-block;
                width: 150px;
                height: 40px;
                line-height: 40px;
                text-align: center;
                font-size: 16px;
                font-weight: bold;
                text-decoration: none;
                color: #fff;
            }

            .button-group button {
                background-color: blue;
                border: none;
                border-radius: 5px;
                margin-right: 10px;
            }

            .button-group a {
                background-color: green;
                border-radius: 5px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <main id="main" class="main">
                
                <div class="pagetitle">
                    <h1>Thông tin cá nhân</h1>
                </div><!-- End Page Title -->
                <div class="tab-content pt-2">
                    <h2 style="color: red">${alert}</h2>
                    <form action="changepassword?account=${sessionScope.acc.user}" method="post" onsubmit="return validatePassword();">


                        <div class="tab-pane fade show active profile-overview" id="profile-edit">
                            <div class="row mb-3">
                                <label for="fullName" class="col-md-4 col-lg-3 col-form-label">ID</label>
                                <div class="col-md-8 col-lg-9">
                                    <input name="" disabled readonly type="text" class="form-control" id="fullName" value="${sessionScope.acc.id}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Tài Khoản</label>
                            <div class="col-md-8 col-lg-9">
                                <input name="" disabled readonly="" type="text" class="form-control" id="fullName" value="${sessionScope.acc.user}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nhập mật khẩu hiện tại</label>
                            <div class="col-md-8 col-lg-9">
                                <input name="oldpass" type="password" class="form-control" id="fullName">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nhập mật khẩu mới</label>
                            <div class="col-md-8 col-lg-9">
                                <input name="newpass" type="password" class="form-control" id="newpass">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nhập lại mật khẩu mới</label>
                            <div class="col-md-8 col-lg-9">
                                <input name="newpass1" type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="button-group">
                        <button type="submit" class="">Xác nhận</button>
                        <a href="home" class="">Quay Lại</a>
                    </div>
                </form>

            </div>
        </main>
 
<!-- End #main -->


        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>
<script>
    function validatePassword() {
        var newPassword = document.getElementById("newpass").value;

        // Regular expressions for validation
        var hasUpperCase = /[A-Z]/.test(newPassword);
        var hasLowerCase = /[a-z]/.test(newPassword);
        var hasNumber = /\d/.test(newPassword);
        var isValidLength = newPassword.length >= 8;

        if (!hasUpperCase || !hasLowerCase || !hasNumber || !isValidLength) {
            alert("Mật khẩu mới phải chứa ít nhất 8 ký tự, bao gồm chữ hoa, chữ thường và số.");
            return false;
        }

        return true;
    }
</script>
    </body>
</html>

<%-- 
    Document   : change-pass
    Created on : Jun 27, 2023, 11:43:31 PM
    Author     : quang
--%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vn">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Quên mật khẩu</title>
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <main>
            <c:if test="${arlet !=null}">
                <div class="alert">
                    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                    <strong>Thông Báo!</strong> ${arlet}
                </div>
            </c:if>
            <div class="container">

                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-6 d-flex flex-column align-items-center justify-content-center">
                                <div class="d-flex justify-content-center py-4">
                                    <a href="" class="logo d-flex align-items-center w-auto">
                                        <img src="assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">Card Online</span>
                                    </a>
                                </div><!-- End Logo -->

                                <div class="card mb-3">
                                    <div class="card-body">
                                        <div class="pt-4 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Quên mật khẩu</h5>
                                        </div>
                                        <form class="row g-4 needs-validation" action="changepass" method="post">
                                            <div class="col-12">
                                                <label class="form-label">Mật khẩu mới</label>
                                                <div class="input-group ">
                                                    <input type="text" name="pass" value="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">Nhập lại mật khẩu</label>
                                                <div class="input-group ">
                                                    <input type="text" name="repass" value="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <label class="form-label">OTP</label>
                                                <div class="input-group ">
                                                    <input type="text" name="otp" value="" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button class="btn btn-primary w-100" type="submit">Xác nhận</button>
                                            </div>

                                        </form>
                                        <div class="col-12">
                                            <p class="small mb-2"><a href="forget">Quay lại</a></p>
                                        </div>
<!--                                        <img id="captchaImage" src="${test}" alt="Captcha Image" />
                                        <br />
                                        <button onclick="refreshCaptcha()">Làm mới Captcha</button>-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </main><!-- End #main -->
        <script type="text/javascript">
            function refreshCaptcha() {
                // Gửi yêu cầu AJAX để làm mới captcha
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        // Thay đổi dữ liệu ảnh captcha trực tiếp trong thẻ <img>
                        document.getElementById("captchaImage").src = "data:image/png;base64," + this.responseText;
                    }
                };
                xhttp.open("GET", "captcha-refresh", true);
                xhttp.send();
            }
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="assets/js/main1.js"></script>
    </body>
</html>
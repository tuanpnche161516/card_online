<%-- 
    Document   : test1
    Created on : Jul 20, 2023, 5:06:15 PM
    Author     : quang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Shopping</title>
    </head>
    <body>
        <form id="cartForm">
            <input type="hidden" name="productId" value="1">
            <input type="number" name="quantity" value="1" min="1">
            <button type="submit" id="addToCartButton">Mua hàng</button>
        </form>
        <script src="script.js"></script>
        <script>
            const cartForm = document.getElementById("cartForm");
            const addToCartButton = document.getElementById("addToCartButton");
            const cartTotal = document.getElementById("cartTotal");

            cartForm.addEventListener("submit", function (event) {
                event.preventDefault();
                const formData = new FormData(cartForm);
                const xhr = new XMLHttpRequest();
                xhr.open("POST", "addToCartServlet", true);
                xhr.onload = function () {
                    if (xhr.status === 200) {
                        const response = JSON.parse(xhr.responseText);
                        cartTotal.textContent = response.total;
                    }
                };
                xhr.send(formData);
            });
        </script>
    </body>
</html>
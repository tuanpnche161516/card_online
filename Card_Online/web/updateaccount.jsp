<%-- 
    Document   : update-product
    Created on : Jun 28, 2023, 10:19:27 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Cập nhật tài khoản</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <main id="main" class="main">
                <section class="section">
                    <div class="row">
                        <div class="col-lg-8">

                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Thông tin sản phẩm</h5>
                                    <!-- General Form Elements -->
                                    <form action="updateaccount" method="post">
                                        <div class="row mb-3">
                                            <label for="" class="col-sm-2 col-form-label">ID</label>
                                            <div class="col-sm-10">
                                                <input type="text" readonly="" name="id" class="form-control" value="${detail.id}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Tài Khoản</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="account" class="form-control" value="${detail.user}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="" class="col-sm-2 col-form-label">Mật Khẩu</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="pass" class="form-control" value="${detail.password}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text"  name="email" class="form-control" value="${detail.email}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="" class="col-sm-2 col-form-label">Ví</label>
                                        <div class="col-sm-10">
                                            <input type="text"  name="money" class="form-control" value="${detail.money}">
                                        </div>
                                    </div>    
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label">Quản trị viên</label>
                                        <div class="col-sm-10">
                                            <select  class="form-select"  name="isAdmin" aria-label="Default select example">
                                                <c:if test="${detail.isAdmin == 1}">
                                                    <option value="1">Đang làm</option>
                                                    <option value="1">Huỷ bỏ</option>
                                                </c:if>
                                                <c:if test="${detail.isAdmin == -1}">
                                                    <option value="-1">Chưa làm</option>
                                                    <option value="1">Cấp làm</option>
                                                </c:if>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label">Kích hoạt</label>
                                        <div class="col-sm-10">
                                            <select  class="form-select"  name="isActive" aria-label="Default select example">
                                                <c:if test="${detail.isActive == 1}">
                                                    <option value="1">Đã kích hoạt</option>
                                                    <option value="-1">Chưa kích hoạt</option>
                                                </c:if>
                                                <c:if test="${detail.isActive == -1}">
                                                    <option value="-1">Chưa Kích hoạt</option>
                                                    <option value="1">Kích hoạt</option>
                                                </c:if>   
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="row mb-3">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                                        </div>
                                    </div>
                                </form><!-- End General Form Elements -->
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary"><a  href="manageraccount" style="color: white;">Quay lại</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>
        <script>
            // Lấy đối tượng ô nhập liệu
            let moneyInput = document.querySelector('input[name="money"]');
            let money1;
// Định dạng giá trị mặc định của ô nhập liệu thành tiền tệ
            let formatter = new Intl.NumberFormat('vi-VN', {style: 'currency', currency: 'VND'});
            moneyInput.value = formatter.format(moneyInput.value);

// Xử lý sự kiện khi người dùng nhập giá trị vào ô nhập liệu
            moneyInput.addEventListener('input', function (event) {
                // Lấy giá trị nhập vào ô nhập liệu
                let money = event.target.value;

                // Loại bỏ các ký tự không phải số khỏi giá trị nhập vào
                money = money.replace(/[^\d]/g, '');

                // Định dạng giá trị nhập vào thành tiền tệ
                let formattedMoney = formatter.format(money);

                // Gán giá trị đã định dạng vào ô nhập liệu
                event.target.value = formattedMoney;
                money1 = money;
            });

// Xử lý sự kiện khi người dùng bấm phím trên bàn phím
            moneyInput.addEventListener('keydown', function (event) {
                // Lấy mã phím của sự kiện
                let keyCode = event.keyCode || event.which;

                // Nếu mã phím là 8 (phím xoá) hoặc 46 (phím xoá trên bàn phím số)
                if (keyCode === 8 || keyCode === 46) {
                    // Xoá giá trị trong ô input
                    moneyInput.value = money1;
                }
            });
        </script>
    </body>

</html>
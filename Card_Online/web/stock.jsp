<%-- 
    Document   : managerproduct
    Created on : Jun 28, 2023, 1:32:26 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Tables / Data - NiceAdmin Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/Stockcss.css"/>
        <style>
            /* Thay đổi font và cỡ chữ */
            /* Thay đổi font và cỡ chữ */
            body {
                font-family: Arial, sans-serif;
                font-size: 14px;
            }

            .container {
                margin: 20px auto;
                max-width: 800px;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 8px; /* Góc bo tròn cho khung container */
            }

            .card {
                padding: 20px;
            }

            .header-buttons .button {
                display: inline-block;
                padding: 8px 16px;
                background-color: #0d6efd;
                color: #fff;
                text-decoration: none;
                border-radius: 4px;
            }

            .header-buttons .button-primary {
                background-color: #007bff;
            }

            .header-buttons h4 {
                margin-top: 10px;
            }

            .table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            .table th,
            .table td {
                padding: 10px;
                text-align: center;
                border: 1px solid #ccc;
            }

            .table th {
                background-color: #f2f2f2;
            }

            .col-action button {
                display: inline-block;
                padding: 5px 10px;
                background-color: #f2f2f2;
                border: 1px solid #ccc;
                color: #0d6efd;
                text-decoration: none;
                border-radius: 4px;
                margin-right: 5px;
            }

            .col-action button.update {
                background-color: #17a2b8; /* Màu xanh nút cập nhật */
                color: #fff;
            }

            .col-action button.delete {
                background-color: #dc3545; /* Màu đỏ nút xóa */
                color: #fff;
            }

            .col-action button:hover {
                background-color: #ccc;
            }

            .pagination {
                list-style: none;
                display: flex;
                justify-content: center;
                margin-top: 20px;
                padding: 0;
            }

            .pagination li {
                margin: 0 5px;
            }

            .pagination li a {
                display: inline-block;
                padding: 5px 10px;
                background-color: #f2f2f2;
                color: #0d6efd;
                text-decoration: none;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .pagination li.active a {
                background-color: #0d6efd;
                color: #fff;
            }
            /* Reset default browser styles */
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }

            /* Body styles (optional) */
            body {
                font-family: Arial, sans-serif;
                background-color: #f7f7f7;
            }

            /* Container styles */
            .container {
                max-width: 800px;
                margin: 0 auto;
                padding: 20px;
            }

            /* Card styles */
            .card {
                background-color: #fff;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 20px;
            }

            /* Button styles */
            .button {
                display: inline-block;
                padding: 10px 20px;
                text-decoration: none;
                color: #fff;
                background-color: #007bff;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
            }

            .button:hover {
                background-color: #0056b3;
            }

            .button-primary {
                background-color: #007bff;
            }

            /* Total styles */
            .total1 {
                display: flex;
                justify-content: space-between;
                align-items: center;
                margin-bottom: 20px;
            }

            .total h4 {
                font-size: 18px;
                font-weight: bold;
            }

            /* File styles */
            .file {
                float: right;
            }

            /* Form styles */
            form {
                display: flex;
                align-items: center;
            }

            label {
                margin-right: 10px;
            }

            input[type="file"] {
                margin-right: 10px;
            }

            /* Select styles */
            select {
                padding: 5px;
                border: 1px solid #ccc;
                border-radius: 3px;
                background-color: #fff;
                margin-right: 10px;
            }

            /* Pagination styles */
            .pagination {
                margin-top: 20px;
                display: flex;
                justify-content: center;
            }

            /* Button styles for pagination */
            #next-page,
            #prev-page,
            #clear-filters {
                padding: 8px 16px;
                background-color: #007bff;
                border: none;
                color: #fff;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
                margin: 0 5px;
            }

            #next-page:hover,
            #prev-page:hover,
            #clear-filters:hover {
                background-color: #0056b3;
            }

            /* Additional spacing for labels */
            div > label {
                margin-right: 10px;
            }


        </style>
    </head>

    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>

            <main id="main" class="main">
                <section class="section">
                    <div id="alert" class="alert" style="display: none">
                        <span class="closebtn">&times;</span> 
                        <strong>Thông Báo!</strong> 
                    </div>
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div class="header-buttons">
                                    <a href="addStock" class="button button-primary">Thêm Sản Phẩm</a>   
                                    <div class="total1" style="display: flex;">
                                        <div class="total">
                                            <h4>Sản Phẩm Trong Kho: ${count}</h4>                                     
                                    </div>                                        
                                </div>
                                <div class="file" style="float: right;">
                                    <form id="myForm">
                                        <label for="file">Chọn File:</label>
                                        <input type="file" id="file" name="file"><br>
                                        <select id="mySelect1" name="typecard">
                                            <option value="1">Viettel</option>
                                            <option value="2">MobiFone</option>
                                            <option value="3">VinaPhone</option>
                                            <option value="4">Vietnamobile</option>
                                            <option value="5">Gate</option>
                                            <option value="6">Zing</option>
                                        </select>
                                        <select id="mySelect2" name="valuecard">
                                            <option value="10000">10000</option>
                                            <option value="20000">20000</option>
                                            <option value="50000">50000</option>
                                            <option value="100000">100000</option>
                                            <option value="200000">200000</option>
                                            <option value="500000">500000</option>
                                        </select>
                                        <input type="button" onclick="submitForm()" value="Submit">
                                    </form>
                                </div>
                            </div>

                            <div>
                                <label for="filter-price">Lọc sản phẩm theo giá trị:</label>
                                <select id="filter-price">
                                    <option value="0">Tất cả</option>
                                    <option value="10000">10,000 đ</option>
                                    <option value="20000">20,000 đ</option>
                                    <option value="50000">50,000 đ</option>
                                    <option value="100000">100,000 đ</option>
                                    <option value="200000">200,000 đ</option>
                                    <option value="500000">500,000 đ</option>
                                </select>
                            </div>
                            <div>
                                <label for="filter-network">Lọc sản phẩm theo nhà mạng:</label>
                                <select id="filter-network">
                                    <option value="0">Tất cả</option>
                                    <option value="1">Viettel</option>
                                    <option value="2">MobiFone</option>
                                    <option value="3">VinaPhone</option>
                                    <option value="4">Vietnamobile</option>
                                    <option value="5">Gate</option>
                                    <option value="6">Zing</option>
                                </select>
                            </div>
                            <div>
                                <label for="filter-status">Lọc sản phẩm đã dùng:</label>
                                <select id="filter-status">
                                    <option value="-1">Tất Cả</option>
                                    <option value="1">chưa dùng</option>
                                    <option value="0">Đã dùng</option>
                                </select>
                            </div>
                            <div>
                                <label for="page-size-select">Số sản phẩm trên trang:</label>
                                <select id="page-size-select">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                </select>
                            </div>
                            <button id="clear-filters">Xoá filter</button>
                            <div id="pagination" class="pagination"></div>
                            <div class="page" style="display: flex;">
                            <div>
                                <button id="next-page">Trang tiếp theo</button>
                            </div>
                            <div>
                                <button id="prev-page">Trang trước</button>
                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->

        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <script src="assets/js/main1.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
                                            function submitForm() {
                                                var form = document.getElementById("myForm");
                                                var formData = new FormData(form);
                                                var xhr = new XMLHttpRequest();
                                                xhr.open("POST", "ImportExcel", true);
                                                xhr.onreadystatechange = function () {
                                                    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                                                        var response = JSON.parse(this.responseText);
                                                        if (response.status === "success") {
                                                            $("#alert").removeClass("error").addClass("success").html("Xử lý thành công.").fadeIn();
                                                        } else {
                                                            $("#alert").removeClass("success").addClass("error").html("Xử lý thất bại.").fadeIn();
                                                        }
                                                        console.log(response);
                                                    }
                                                };
                                                xhr.send(formData);
                                            }
                                            $(document).ready(function () {
                                                var currentPage = 1;
                                                $('#filter-price, #filter-network, #filter-status, #page-size-select').on('change', function () {
                                                    var filterPrice = $('#filter-price').val();
                                                    var filterNetwork = $('#filter-network').val();
                                                    var filterStatus = $('#filter-status').val();
                                                    var pageSizeSelect = $('#page-size-select').val();
                                                    loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                                                });
                                                loadProductList('0', '0', currentPage, '-1', '5');
                                                $('#next-page').on('click', function () {
                                                    currentPage++; // Tăng giá trị của currentPage lên 1
                                                    var filterPrice = $('#filter-price').val();
                                                    var filterNetwork = $('#filter-network').val();
                                                    var filterStatus = $('#filter-status').val();
                                                    var pageSizeSelect = $('#page-size-select').val();
                                                    loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                                                });
                                                $('#prev-page').on('click', function () {
                                                    if (currentPage > 1) {
                                                        currentPage--; // Giảm giá trị của currentPage đi 1
                                                        var filterPrice = $('#filter-price').val();
                                                        var filterNetwork = $('#filter-network').val();
                                                        var filterStatus = $('#filter-status').val();
                                                        var pageSizeSelect = $('#page-size-select').val();
                                                        loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect);
                                                    }
                                                });
                                                $('#clear-filters').on('click', function () {
                                                    // Đặt giá trị của các phần tử filter về giá trị mặc định
                                                    $('#filter-price').val('0');
                                                    $('#filter-network').val('0');
                                                    $('#filter-status').val('-1');
                                                    $('#page-size-select').val('5');

                                                    // Tải lại danh sách sản phẩm với filter mặc định
                                                    loadProductList('0', '0', currentPage, '-1', '5');
                                                });
                                            });
                                            function loadProductList(filterPrice, filterNetwork, currentPage, filterStatus, pageSizeSelect) {
                                                $.ajax({
                                                    url: 'FilterStock',
                                                    type: 'GET',
                                                    data: {
                                                        filterPrice: filterPrice,
                                                        filterNetwork: filterNetwork,
                                                        currentPage: currentPage,
                                                        filterStatus: filterStatus,
                                                        pageSizeSelect: pageSizeSelect
                                                    },
                                                    success: function (data) {
                                                        $('#product-list').html(data.products);
                                                        $('#pagination').html(data.pagination);
                                                        $('.page-link').on('click', function () {
                                                            var page = $(this).data('page');
                                                            loadProductList(filterPrice, filterNetwork, page, filterStatus, pageSizeSelect);
                                                        });
                                                    }
                                                });
                                            }
        </script>
    </body>

</html>
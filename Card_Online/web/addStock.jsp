<%-- 
    Document   : update-product
    Created on : Jun 28, 2023, 10:19:27 AM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Forms / Elements - NiceAdmin Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/addstock.css"/>
    </head>
    <style>
        .row {
            display: flex;
            flex-wrap: wrap;
        }

        .row .col-sm-2 {
            flex: 0 0 auto;
            width: 20%;
        }

        .row .col-sm-10 {
            flex: 0 0 auto;
            width: 80%;
        }

        .mb-3.row .col-sm-2.col-form-label {
            padding-top: calc(0.375rem + 1px);
            padding-bottom: calc(0.375rem + 1px);
        }

        .form-select {
            width: 100%;
        }


    </style>
    <body>

        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <!-- ======= Sidebar ======= -->
        <jsp:include page="left.jsp"></jsp:include>
            <!--            <main id="main" class="main">              
                            <section class="section">
                                <div class="row">
                                    <div class="col-lg-8">
            
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">Thêm Vào Kho</h5>
                                                 General Form Elements 
                                                <form action="addStock" method="post">   
                                                    <div class="row mb-3">
                                                        <label for="" class="col-sm-2 col-form-label">Seri Thẻ</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="seri" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="" class="col-sm-2 col-form-label">Mã Thẻ</label>
                                                        <div class="col-sm-10">
                                                            <input type="text"  name="code" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="" class="col-sm-2 col-form-label">Hạn Sử Dụng</label>
                                                        <div class="col-sm-10">
                                                            <input type="date"  name="expiry"class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label class="col-sm-2 col-form-label">Thẻ</label>
                                                        <div class="col-sm-10">
                                                            <select  class="form-select"  name="type" aria-label="Default select example">
        <c:forEach items="${listC}" var="o">
            <option value="${o.id}">${o.name}</option>
        </c:forEach>
    </select>
</div>
</div>
<div class="row mb-3">
<div class="col-sm-10">
    <button type="submit" class="btn btn-primary">Đồng ý</button>
</div>
</div>
</form>
End General Form Elements 
<div class="row mb-3">
<div class="col-sm-10">
<button type="submit" class="btn btn-primary"><a href="stock" style="color: white;">Quay lại</a></button>
</div>
</div>
</div>
</div>

</div>

</div>
</section>
</main> End #main -->
        <main id="main" class="main">
            <section class="section">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Thêm Vào Kho</h5>
                                <!-- General Form Elements -->
                                <form action="newStock" method="post">
                                    <div class="mb-3 row">
                                        <label for="" class="col-sm-2 col-form-label">Seri Thẻ</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="seri" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="" class="col-sm-2 col-form-label">Mã Thẻ</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="code" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="" class="col-sm-2 col-form-label">Tên Thẻ</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="" class="col-sm-2 col-form-label">Mệnh giá</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="price" class="form-control">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="" class="col-sm-2 col-form-label">Hạn Sử Dụng</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="expiry" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label">Thẻ</label>
                                        <div class="col-sm-10">
                                            <select  class="form-select"  name="type" aria-label="Default select example">
                                                <c:forEach items="${listC}" var="o">
                                                    <option value="${o.id}">${o.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- End General Form Elements -->
                                <div class="mb-3 row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">
                                            <a href="stock" style="color: white;">Quay lại</a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->


        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Template Main JS File -->
        <script src="assets/js/main1.js"></script>

    </body>

</html>
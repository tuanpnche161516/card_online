<%-- 
    Document   : left
    Created on : Jun 26, 2023, 11:20:22 PM
    Author     : quang
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="home">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->
        <c:if test="${sessionScope.acc != null}">
            <li class="nav-item">
                <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <c:forEach items="${listC}" var="o">
                        <li>
                            <a href="selectcategory?cid=${o.id}">
                                <i class="bi bi-circle"></i><span>${o.name}</span>
                            </a>
                        </li>
                    </c:forEach>

                </ul>
            </li><!-- End Components Nav -->
        </c:if>

        <c:if test="${acc.isAdmin == 1}">
            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-journal-text"></i><span>Quản lý</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="manageraccount">
                            <i class="bi bi-circle"></i><span>Quản lý tài khoản</span>
                        </a>
                    </li>
                    <li>
                        <a href="manageProduct">
                            <i class="bi bi-circle"></i><span>Quản lý sản phẩm</span>
                        </a>
                    </li>
                    <li>
                        <a href="stock">
                            <i class="bi bi-circle"></i><span>Kho hàng</span>
                        </a>
                    </li>
                    <li>
                        <a href="transitionAdmin">
                            <i class="bi bi-circle"></i><span>Xem Các Giao Dịch</span>
                        </a>
                    </li>
                    <li>
                        <a href="oderCard">
                            <i class="bi bi-circle"></i><span>Xem Đơn Đặt Hàng</span>
                        </a>
                    </li>
                </ul>
            </li><!-- End Forms Nav -->
        </c:if>


        <c:if test="${acc != null}">
            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-layout-text-window-reverse"></i><span>Giao dịch</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="transitionUser">
                            <i class="bi bi-circle"></i><span>Lịch sử giao dịch</span>
                        </a>
                    </li>
                    <li>
                        <a href="oderCardUser">
                            <i class="bi bi-circle"></i><span>Xem Đơn Đặt Hàng</span>
                        </a>
                    </li>
<!--                    <li>
                        <a href="cart?Acc=${sessionScope.acc.user}">
                            <i class="bi bi-circle"></i><span>Giỏ hàng</span>
                        </a>
                    </li>-->
                </ul>
            </li><!-- End Tables Nav -->
        </c:if>



        <li class="nav-heading">Pages</li>
            <c:if test="${acc != null}">
            <li class="nav-item">
                <a class="nav-link collapsed" href="profile">
                    <i class="bi bi-person"></i>
                    <span>Trang cá nhân</span>
                </a>
            </li><!-- End Profile Page Nav -->
        </c:if>


<!--        <li class="nav-item">
            <a class="nav-link collapsed" href="">
                <i class="bi bi-question-circle"></i>
                <span>F.A.Q</span>
            </a>
        </li> End F.A.Q Page Nav 

        <li class="nav-item">
            <a class="nav-link collapsed" href="">
                <i class="bi bi-envelope"></i>
                <span>Liên hệ</span>
            </a>
        </li> End Contact Page Nav -->
        <c:if test="${sessionScope.acc == null}">
            <li class="nav-item">
                <a class="nav-link collapsed" href="sign">
                    <i class="bi bi-card-list"></i>
                    <span>Đăng  kí</span>
                </a>
            </li><!-- End Register Page Nav -->

            <li class="nav-item">
                <a class="nav-link collapsed" href="login">
                    <i class="bi bi-box-arrow-in-right"></i>
                    <span>Đăng nhập</span>
                </a>
            </li>
        </c:if>

        <!-- End Login Page Nav -->

        <!--        <li class="nav-item">
                    <a class="nav-link collapsed" href="pages-error-404.html">
                        <i class="bi bi-dash-circle"></i>
                        <span>Error 404</span>
                    </a>
                </li> End Error 404 Page Nav -->

        <!--        <li class="nav-item">
                    <a class="nav-link collapsed" href="pages-blank.html">
                        <i class="bi bi-file-earmark"></i>
                        <span>Blank</span>
                    </a>
                </li> End Blank Page Nav -->

    </ul>

</aside>
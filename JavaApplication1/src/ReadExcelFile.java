
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFile {

    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream(new File("thecao10k.xlsx"));
        // Tạo đối tượng XSSFWorkbook để đọc workbook
        XSSFWorkbook workbook = new XSSFWorkbook(fis);

        // Lấy sheet đầu tiên từ workbook
        Sheet sheet = workbook.getSheetAt(0);

        // Lặp qua từng hàng trong sheet và đọc dữ liệu từ từng ô
        for (Row row : sheet) {
            for (Cell cell : row) {
                // In giá trị của từng ô vào console
                System.out.print(cell.getStringCellValue() + "\t");
            }
            System.out.println();
        }

        // Đóng FileInputStream và workbook
        fis.close();
        workbook.close();

//        try {
//            // Tạo đối tượng Workbook
//            XSSFWorkbook workbook = new XSSFWorkbook();
//            // Tạo đối tượng Sheet
//            Sheet sheet = workbook.createSheet("Sheet1");
//            for (int i = 0; i < 300; i++) {
//                Row row = sheet.createRow(i);
//                Cell cell = row.createCell(0);
//                cell.setCellValue(String.valueOf(i + 1));
//                cell = row.createCell(1);
//                int Number = ThreadLocalRandom.current().nextInt(111111, 999999); // Sinh số ngẫu nhiên trong phạm vi từ 100 đến 200
//                String code = "31231" + String.valueOf(Number);
//                cell.setCellValue(String.valueOf(code));
//                cell = row.createCell(2);
//                Number = ThreadLocalRandom.current().nextInt(111111, 999999); // Sinh số ngẫu nhiên trong phạm vi từ 100 đến 200
//                code = "12341" + String.valueOf(Number);
//                cell.setCellValue(String.valueOf(code));
//                cell = row.createCell(3);
//                Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(currentTimestamp);
//                calendar.add(Calendar.YEAR, 10);
//                Timestamp newTimestamp = new Timestamp(calendar.getTimeInMillis());
//                cell.setCellValue(String.valueOf(newTimestamp));
//            }
//            FileOutputStream fos = new FileOutputStream("thecao10k.xlsx", false);
//            workbook.write(fos);
//            System.out.println("Đã ghi dữ liệu vào file Excel thành công!");
//        } catch (IOException e) {
//            System.out.println("Đã xảy ra lỗi khi ghi dữ liệu vào file Excel: " + e.getMessage());
//        }
    }
}
